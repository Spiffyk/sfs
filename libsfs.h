/**@file
 * @brief The high-level SFS library.
 *
 * This library contains high-level functionality for interfacing with the filesystem.
 */

#ifndef SMORKFS_LIBSFS_H
#define SMORKFS_LIBSFS_H

#include <stdint.h>

#include "libsfscommon.h"

/**
 * Generates sane default formatting parameters for the specified SFS volume.
 *
 * @param [in] fs the volume to generate sane defaults for
 *
 * @return formatting parameters
 */
sfs_format_params sfs_format_params_gendefaults(sfs *fs);

/**
 * Opens an SFS volume.
 *
 * @param [in] path
 *          The path to the volume containing the filesystem
 *
 * @return a reference to the SFS volume
 */
sfs *sfs_volume_open(char *path);

/**
 * Closes an SFS volume and frees the resources associated with it.
 *
 * @param [out] target
 *          The volume to close
 */
void sfs_volume_close(sfs **fs);

/**
 * Checks if the SFS volume is valid.
 *
 * @param [in] fs
 *          The volume to check
 *
 * @return non-zero value if valid, zero if invalid
 */
int sfs_volume_valid(sfs *fs);

/**
 * Formats the specified volume according to the specified parameters.
 *
 * @param [in] fs
 *          Reference to the volume to format
 * @param [in] params
 *          The formatting parameters
 *
 * @return current SFS status
 */
sfs_status sfs_format(sfs *fs, sfs_format_params *params);

/**
 * Checks whether the entry on the specified path is a directory.
 *
 * @param [in] fs
 *          The volume to create the directory in
 * @param [in] path
 *          The path to the entry
 *
 * @return non-zero if the entry is a directory, zero otherwise
 */
int sfs_isdir(sfs *fs, const char *path);

/**
 * Creates a new directory in the filesystem.
 *
 * @param [in] fs
 *          The volume to create the directory in
 * @param [in] path
 *          The path to the directory
 *
 * @return current SFS status
 */
sfs_status sfs_mkdir(sfs *fs, const char *path);

/**
 * Creates a new directory in the filesystem.
 *
 * @param [in] fs
 *          The volume to create the directory in
 * @param [in] path
 *          The path to the directory
 *
 * @return current SFS status
 */
sfs_status sfs_rmdir(sfs *fs, const char *path);

/**
 * Removes the specified entry from the filesystem.
 *
 * @param [in] fs
 *          The volume to remove the entry from
 * @param [in] path
 *          The path to the entry
 *
 * @return current SFS status
 */
sfs_status sfs_rm(sfs *fs, const char *path);

/**
 * Moves the specified source entry to the specified destination location.
 *
 * @param [in] fs
 *          The volume to move the entry in
 * @param [in] srcpath
 *          The path to the entry
 * @param [in] dstpath
 *          The path to the destination
 *
 * @return current SFS status
 */
sfs_status sfs_mv(sfs *fs, const char *srcpath, const char *dstpath);

/**
 * Lists the entries of the specified directory.
 *
 * @param [in] fs
 *          The volume
 * @param [in] path
 *          The path to the directory
 *
 * @return current SFS status
 */
sfs_status sfs_ls(sfs *fs, const char *path);

/**
 * Prints out info about the specified file/directory.
 *
 * @param [in] fs
 *          The volume
 * @param [in] path
 *          The path to the file/directory
 *
 * @return current SFS status
 */
sfs_status sfs_info(sfs *fs, const char *path);

/**
 * Opens a stream on the file with the specified path.
 *
 * @param [in] fs
 *          The volume to open the stream in
 * @param [in] path
 *          The path to the file to open
 * @param [in] mode
 *          The mode to open the stream in
 *
 * @return a reference to a stream on success, @ref NULL otherwise
 */
sfs_stream *sfs_open(sfs *fs, const char *path, sfs_stream_mode mode);

/**
 * Flushes and closes the specified stream.
 *
 * @param [out] stream
 *          The stream to close
 */
void sfs_close(sfs_stream *stream);

/**
 * Moves the seek position of the specified stream.
 *
 * @param [out] stream
 *          The stream to seek in
 * @param [in] seek
 *          The amount of bytes to move by
 * @param [in] type
 *          The type of seek, may be @ref SFS_SEEK_CUR, @ref SFS_SEEK_SET or @ref SFS_SEEK_END
 */
void sfs_seek(sfs_stream *stream, long seek, sfs_seek_type type);

/**
 * Gets the current seek position of the specified stream.
 *
 * @param [in] stream
 *          The stream whose position is to be found
 *
 * @return the current seek position or `-1` if invalid
 */
long sfs_tell(const sfs_stream *stream);

/**
 * Checks whether the specified stream's seek position is at the end of the file.
 *
 * @param [in] stream
 *          The stream whose EOF status is to be checked
 *
 * @return `1` if the stream is at the end of the file, otherwise `0`
 */
int sfs_eof(const sfs_stream *stream);

/**
 * Discards everything on and after the current seek position of the writable stream.
 *
 * @param [out] stream
 *          The stream to write into
 *
 * @return the new size of the file
 */
long sfs_trim(sfs_stream *stream);

/**
 * Writes `elem_count` elements of `elem_size` bytes from `src` into the specified writable stream.
 *
 * @param [in] src
 *          The starting pointer of the data to be written
 * @param [in] elem_size
 *          The size of a single element
 * @param [in] elem_count
 *          The number of elements
 * @param [out] stream
 *          The stream to write into
 *
 * @return the number of successfully written elements
 */
size_t sfs_write(const void* src, size_t elem_size, size_t elem_count, sfs_stream *stream);

/**
 * Reads `elem_count` elements of `elem_size` bytes from the specified readable stream into `dest`.
 *
 * @param [out] dest
 *          The starting pointer of the buffer to put the read data in
 * @param [in] elem_size
 *          The size of a single element
 * @param [in] elem_count
 *          The number of elements
 * @param [out] stream
 *          The stream to read from
 *
 * @return the number of successfully read elements
 */
size_t sfs_read(void* dest, size_t elem_size, size_t elem_count, sfs_stream *stream);

/**
 * Checks the filesystem for inconsistencies and prints out the results.
 *
 * @param [in] fs
 *          The volume to check
 */
void sfs_fsck(sfs *fs);

/**
 * Allocates an orphaned inode for fsck to find.
 *
 * @param [in] fs
 *          The volume in which the orphaned inode is to be created
 */
void sfs_fsckdbg_alloc_orphan(sfs *fs);

/**
 * Allocates an inode with an inadequate number of blocks.
 *
 * @param [in] fs
 *          The volume in which the inode is to be created
 */
void sfs_fsckdbg_alloc_inadequate_blocks(sfs *fs);

#endif /* SMORKFS_LIBSFS_H */
