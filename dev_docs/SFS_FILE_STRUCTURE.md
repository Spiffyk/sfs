# File structure of SFS

All filesystem-specific structures are to be stored in *Big Endian* - use `libsfs_conv` and `liblowsfs_conv`
to perform conversions.

## Main sequence

| Structure data type | Count                     | Description                     |
|:--------------------|:--------------------------|:--------------------------------|
| `lsfs_header`       | `1`                       | Header file                     |
| `lsfs_inode`        | `num_inodes`              | Inodes, first is root directory |
| `uint8_t`           | `num_blocks / 8`          | Data block usage bitmap         |
| `uint8_t`           | `num_blocks * block size` | Data blocks                     |

## Identifications

* **inode** - `lsfs_inode_id_t` - starts with `0`; `1` is the root inode, `0` is reserved as a null-reference
* **block** - `lsfs_block_id_t` - starts with `1`; `0` is reserved as a null-reference
