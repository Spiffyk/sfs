# File structure for a SFS directory

All filesystem-specific structures are to be stored in *Big Endian* - use `libsfs_conv` and `liblowsfs_conv`
to perform conversions.

## Main sequence

| Data type           | Count                     | Description                                                  |
|:--------------------|:--------------------------|:-------------------------------------------------------------|
| `sfs_dir_magic_t`   | `1`                       | Magic number (for structure versioning, if needed in future) |
| `sfs_dir_entry`     | *any*                     | Directory entries, concluded by `\0` (empty name)            |
