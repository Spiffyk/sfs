function init() {
    var inode = struct({
        type: uint8(),
        ref_count: uint32().set({ byteOrder: "big-endian" }),
        size: uint64().set({ byteOrder: "big-endian" }),
        blockrefs_d: array(uint64().set({ byteOrder: "big-endian" }), 6),
        blockrefs_i1: uint64().set({ byteOrder: "big-endian" }),
        blockrefs_i2: uint64().set({ byteOrder: "big-endian" })
    }).set({
        name: "inode"
    });

    return struct({
        header: struct({
            magic: array(uint8(), 4),
            inode_count: uint32().set({ byteOrder: "big-endian" }),
            block_size: uint32().set({ byteOrder: "big-endian" }),
            block_count: uint64().set({ byteOrder: "big-endian" })
        }),
        inodes: array(inode, function() { return this.parent.header.inode_count.value; }),
        bitmap: array(uint8(), function() { return Math.ceil(this.parent.header.block_count.value / 8); }),
        blocks: array(array(uint8(), function() { return this.parent.parent.header.block_size.value }), function() { return this.parent.header.block_count.value })
    }).set({
        name: "sfs",
        defaultLockOffset: 0x0
    });
}
