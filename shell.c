#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <log.h>

#include "libsfs.h"

#define MAX_PATH_LENGTH (256)
#define MAX_CMD_NAME_LENGTH (16)
#define MAX_CMD_LENGTH (1024)
#define MAX_CMD_ARGS (64)
#define CP_BUFFER_SIZE (1024)


typedef int (*cmd_handler)(int argc, char **argv);

typedef struct cmd_entry_s {
    char name[MAX_CMD_NAME_LENGTH];
    cmd_handler handler;
    struct cmd_entry_s *next;
} cmdr_entry;

typedef struct {
    cmdr_entry *first;
} cmdr;


int cmd_dummy_main(int argc, char **argv);
int cmd_exit_main(int argc, char **argv);
int cmd_argdump_main(int argc, char **argv);
int cmd_echo_main(int argc, char **argv);
int cmd_load_main(int argc, char **argv);
int cmd_format_main(int argc, char **argv);
int cmd_ls_main(int argc, char **argv);
int cmd_cd_main(int argc, char **argv);
int cmd_mkdir_main(int argc, char **argv);
int cmd_rmdir_main(int argc, char **argv);
int cmd_pwd_main(int argc, char **argv);
int cmd_info_main(int argc, char **argv);
int cmd_mv_main(int argc, char **argv);
int cmd_rm_main(int argc, char **argv);
int cmd_cp_main(int argc, char **argv);
int cmd_incp_main(int argc, char **argv);
int cmd_outcp_main(int argc, char **argv);
int cmd_cat_main(int argc, char **argv);
int cmd_fsck_main(int argc, char **argv);
int cmd_break_orphan_main(int argc, char **argv);
int cmd_break_iblock_main(int argc, char **argv);


cmdr *cmdr_create();
void cmdr_destroy(cmdr **c_ptr);
void cmdr_add(cmdr *c, const char *name, cmd_handler handler);
int cmdr_handle_next_command(cmdr *c);
int cmdr_handle_command(cmdr *c, const char *buffer, const char *sherr_prefix);
void insert_sfs_commands(cmdr *c);

unsigned long parse_num(const char *str);
unsigned long parse_size(const char *str);
int ensure_fs();
void pathcat(char *dest, const char *path);
int errcheck(int rv, int err_rv);
int final_errcheck(int err_rv);


char pwd[MAX_PATH_LENGTH] = {0};
char volume_path[MAX_PATH_LENGTH] = {0};
sfs *fs = NULL;
int exit_requested = 0;


int main(int argc, char **argv) {
    if (argc < 1) {
        printf("There was a problem with the argument count. This should not be happening.\n");
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <path_to_volume>\n", argv[0]);
        return 1;
    }

    log_set_level(LOG_FATAL);

    strcpy(pwd, "/");
    strcpy(volume_path, argv[1]);
    printf("Welcome to the SFS Shell - Volume: %s\n", volume_path);

    cmdr *c = cmdr_create();
    if (!c) {
        printf("Error creating command handler\n");
        return 2;
    }
    insert_sfs_commands(c);

    while (!exit_requested) {
        sfs_reseterr();
        int retval = cmdr_handle_next_command(c);

        if (retval) {
            printf("Command exited with code %d\n\n", retval);
        }
    }

    if (fs) {
        sfs_volume_close(&fs);
    }
    cmdr_destroy(&c);
}

cmdr *cmdr_create() {
    cmdr *c = malloc(sizeof(cmdr));
    if (!c) {
        return NULL;
    }

    c->first = NULL;
    return c;
}

void cmdr_destroy(cmdr **c_ptr) {
    if (!c_ptr || !*c_ptr) {
        return;
    }

    cmdr *c = *c_ptr;

    if (c->first) {
        cmdr_entry *next = c->first;
        do {
            cmdr_entry *current = next;
            next = next->next;
            free(current);
        } while (next);
    }

    free(c);
    *c_ptr = NULL;
}

void cmdr_add(cmdr *c, const char *name, cmd_handler handler) {
    cmdr_entry *e = malloc(sizeof(cmdr_entry));
    strncpy(e->name, name, MAX_CMD_NAME_LENGTH);
    e->handler = handler;
    e->next = NULL;

    cmdr_entry **dest = &c->first;
    while (*dest) {
        dest = &(*dest)->next;
    }
    *dest = e;
}

int cmdr_handle_next_command(cmdr *c) {
    char input_buf[MAX_CMD_LENGTH];
    printf("%s > ", pwd);
    fgets(input_buf, MAX_CMD_LENGTH, stdin);
    return cmdr_handle_command(c, input_buf, "");
}

int cmdr_handle_command(cmdr *c, const char *buffer, const char *sherr_prefix) {
    int argc = 0;
    char *argv[MAX_CMD_ARGS] = {0};
    char argv_buf[MAX_CMD_LENGTH] = {0};
    const char *in = buffer;
    char *out = argv_buf;

    int in_quot = 0;
    int esc = 0;

    while(isspace(*in)) {
        in++;
    }

    if (!*in) {
        return 0;
    }

    while (*in) {
        argv[argc] = out;
        while (*in && (!isspace(*in) || in_quot)) {
            if (!esc && *in == '\\') {
                esc = 1;
                in++;
                continue;
            }

            if (in_quot) {
                if (!esc && *in == '"') {
                    in_quot = 0;
                    goto char_end;
                }

                *out = *in;
                out++;
            } else {
                if (esc) {
                    printf("%sSyntax error - invalid escape\n", sherr_prefix);
                    return 2;
                }

                if (*in == '"') {
                    in_quot = 1;
                    goto char_end;
                }

                *out = *in;
                out++;
            }

            char_end:
            esc = 0;
            in++;
        }
        *out = '\0';
        out++;
        argc++;

        while (isspace(*in)) {
            in++;
        }
    }

    if (in_quot) {
        printf("%sSyntax error - unmatched quotes\n", sherr_prefix);
        return 2;
    }

    if (esc) {
        printf("%sSyntax error - escape character not followed by anything\n", sherr_prefix);
        return 2;
    }

    const char *command_name = argv[0];

    cmdr_entry *e = c->first;
    while(e) {
        if (strncmp(command_name, e->name, MAX_CMD_NAME_LENGTH) == 0) {
            return e->handler(argc, argv);
        }
        e = e->next;
    }

    printf("%sUnknown command '%s'\n", sherr_prefix, command_name);
    return 1;
}


void insert_sfs_commands(cmdr *c) {
    // System commands
    cmdr_add(c, "exit", cmd_exit_main);
    cmdr_add(c, "quit", cmd_exit_main);
    cmdr_add(c, "q", cmd_exit_main);
    cmdr_add(c, "#", cmd_dummy_main);

    // Debug commands
    cmdr_add(c, "echo", cmd_echo_main);
    cmdr_add(c, "argdump", cmd_argdump_main);

    // Scripting
    cmdr_add(c, "load", cmd_load_main);

    // FS commands
    cmdr_add(c, "format", cmd_format_main);
    cmdr_add(c, "ls", cmd_ls_main);
    cmdr_add(c, "ll", cmd_ls_main); // I got used to 'll' too much
    cmdr_add(c, "cd", cmd_cd_main);
    cmdr_add(c, "mkdir", cmd_mkdir_main);
    cmdr_add(c, "rmdir", cmd_rmdir_main);
    cmdr_add(c, "pwd", cmd_pwd_main);
    cmdr_add(c, "info", cmd_info_main);
    cmdr_add(c, "mv", cmd_mv_main);
    cmdr_add(c, "rm", cmd_rm_main);
    cmdr_add(c, "cp", cmd_cp_main);
    cmdr_add(c, "incp", cmd_incp_main);
    cmdr_add(c, "outcp", cmd_outcp_main);
    cmdr_add(c, "cat", cmd_cat_main);
    cmdr_add(c, "fsck", cmd_fsck_main);
    cmdr_add(c, "break-orphan", cmd_break_orphan_main);
    cmdr_add(c, "break-iblock", cmd_break_iblock_main);
}

int ensure_fs() {
    if (!fs) {
        fs = sfs_volume_open(volume_path);

        if (!fs) {
            printf("Could not open volume '%s'. Did you forget to format?\n", volume_path);
            return 1;
        }
    }

    if (sfs_volume_valid(fs)) {
        return 0;
    } else {
        printf("Volume is not valid. Did you forget to format?\n");
        return 1;
    }
}

void pathcat(char *dest, const char *path) {
    if (*path == '/') {
        // Query is absolute
        strncpy(dest, path, MAX_PATH_LENGTH);
    } else {
        // Query is relative
        strncpy(dest, pwd, MAX_PATH_LENGTH);
        strncat(dest, path, MAX_PATH_LENGTH);
    }
}

int errcheck(int rv, int err_rv) {
    if (sfs_err()) {
        switch (sfs_err()) {
            case SFS_STATUS_ERR_SPACE:
                printf("NO MORE FREE BLOCKS\n");
                break;
            case SFS_STATUS_ERR_ISPACE:
                printf("NO MORE FREE INODES\n");
                break;
            case SFS_STATUS_ERR_REFNUM:
                printf("TOO MANY BLOCK REFERENCES ON INODE\n");
                break;
            case SFS_STATUS_ERR_LONGNAME:
                printf("NAME TOO LONG\n");
                break;
            case SFS_STATUS_ERR_NEXISTS:
                printf("FILE NOT FOUND\n");
                break;
            case SFS_STATUS_ERR_NPATH:
                printf("PATH NOT FOUND\n");
                break;
            case SFS_STATUS_ERR_NEMPTY:
                printf("NOT EMPTY\n");
                break;
            case SFS_STATUS_ERR_EXISTS:
                printf("EXISTS\n");
                break;
            case SFS_STATUS_ERR_REMSYSTEM:
                printf("CANNOT REMOVE SYSTEM ENTRY\n");
                break;
            case SFS_STATUS_ERR_NDIR:
                printf("NOT A DIRECTORY\n");
                break;
            case SFS_STATUS_ERR_NFILE:
                printf("NOT A DATA FILE\n");
                break;
            default:
                printf("The filesystem threw an error (code 0x%04x)\n", sfs_err());
        }

        return err_rv;
    }

    return rv;
}

int final_errcheck(int err_rv) {
    return errcheck(0, err_rv);
}

unsigned long parse_num(const char *str) {
    unsigned long num = 0;
    if (!isdigit(*str)) {
        printf("Invalid size number\n");
        return 0;
    }
    while (*str) {
        if (!isdigit(*str)) {
            printf("Invalid number\n");
            return 0;
        }
        num *= 10;
        num += *str - '0';
        str++;
    }

    return num;
}

unsigned long parse_size(const char *str) {
    unsigned long size = 0;
    if (!isdigit(*str)) {
        printf("Invalid size number\n");
        return 0;
    }
    while (*str && isdigit(*str)) {
        size *= 10;
        size += *str - '0';
        str++;
    }

    if (*str) {
        switch (toupper(*str)) {
            case 'K':
                size *= 1024;
                break;
            case 'M':
                size *= 1024 * 1024;
                break;
            case 'G':
                size *= 1024 * 1024 * 1024;
                break;
            default:
                printf("Invalid size suffix\n");
                return 0;
        }
    }

    return size;
}


int cmd_dummy_main(int argc, char **argv) {
    return 0;
}

int cmd_exit_main(int argc, char **argv) {
    printf("Exiting the shell...\n");
    exit_requested = 1;
    return 0;
}

int cmd_argdump_main(int argc, char **argv) {
    for (int i = 0; i < argc; i++) {
        printf("%d -> %s\n", i, argv[i]);
    }
    return 0;
}

int cmd_echo_main(int argc, char **argv) {
    if (argc < 2) {
        putchar('\n');
        return 0;
    }
    for (int i = 1; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    return 0;
}

int cmd_load_main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage: %s <phys_script_path>\n", argv[0]);
        return 1;
    }

    const char *script_path = argv[1];
    FILE *f = fopen(script_path, "r");
    if (!f) {
        printf("Could not open script '%s'\n", script_path);
        return 2;
    }

    cmdr *c = cmdr_create();
    if (!c) {
        printf("Error creating command handler\n");
        fclose(f);
        return 2;
    }
    insert_sfs_commands(c);

    int rv = 0;
    unsigned long lineno = 1;
    while (!feof(f)) {
        char prefix[256];
        char buffer[MAX_CMD_LENGTH] = {0};
        fgets(buffer, MAX_CMD_LENGTH, f);

        sprintf(prefix, "[%s:%lu]: ", script_path, lineno);

        rv = cmdr_handle_command(c, buffer, prefix);
        lineno++;

        if (rv) {
            printf("An error occurred in the script (code %d), exiting...\n", rv);
            break;
        }
    }

    fclose(f);
    cmdr_destroy(&c);
    return rv;
}

int cmd_format_main(int argc, char **argv) {
    if (argc < 2) {
        printf("Usage: %s <volume_size> [block_size|0] [inode_count|0]\n", argv[0]);
        return 1;
    }

    unsigned long volume_size = parse_size(argv[1]);
    uint32_t block_size = 0;
    unsigned int inode_count = 0;

    if (argc >= 3) {
        block_size = (uint32_t) parse_size(argv[2]);
    }
    if (argc >= 4) {
        inode_count = (unsigned int) parse_num(argv[3]);
    }

    if (!volume_size) {
        printf("Invalid input\n");
        return 1;
    }

    if (!block_size) {
        block_size = 1024;
    }
    if (!inode_count) {
        inode_count = 1024;
    }

    if (fs) {
        sfs_volume_close(&fs);
    }

    FILE *w = fopen(volume_path, "wb");
    if (!w) {
        printf("Could not open volume for allocation\n");
        return 3;
    }
    unsigned long written = 0;
    for (unsigned long i = 0; i < volume_size; i++) {
        const uint8_t zero = 0;
        written += fwrite(&zero, 1, 1, w);
    }
    int cr = fclose(w);
    if (cr || written != volume_size) {
        printf("Could not allocate\n");
        return 4;
    }

    fs = sfs_volume_open(volume_path);
    if (!fs) {
        printf("Could not open volume after allocation\n");
        return 5;
    }

    sfs_format_params params;
    params.inode_count = inode_count;
    params.block_size = block_size;
    sfs_format(fs, &params);

    return final_errcheck(6);
}

int cmd_ls_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    char dir[MAX_PATH_LENGTH] = {0};
    if (argc >= 2) {
        pathcat(dir, argv[1]);
    } else {
        strncpy(dir, pwd, MAX_PATH_LENGTH);
    }

    sfs_ls(fs, dir);

    return final_errcheck(2);
}

int cmd_cd_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        return 0;
    }

    char catbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(catbuffer, argv[1]);

    if (!sfs_isdir(fs, catbuffer)) {
        printf("NOT A DIRECTORY\n");
        return 2;
    }

    // Remove . and .. from pwd
    char namebuffer[SFS_DIR_ENTRY_NAME_LENGTH] = {0};
    const char *in = catbuffer;
    char *name_out = namebuffer;
    char *pwd_out = pwd;

    while (*in) {
        while (*in && *in != '/') {
            *(name_out++) = *in;
            *(pwd_out++) = *in;
            in++;
        }

        *name_out = '\0';

        if (strcmp(namebuffer, ".") == 0) {
            do {
                pwd_out--;
            } while (*pwd_out != '/' && pwd_out > pwd);
        } else if (strcmp(namebuffer, "..") == 0) {
            do {
                pwd_out--;
            } while (*pwd_out != '/' && pwd_out > pwd);
            do {
                pwd_out--;
            } while (*pwd_out != '/' && pwd_out > pwd);
        }

        if (pwd_out < pwd) {
            pwd_out = pwd;
        }

        *(pwd_out++) = '/';
        name_out = namebuffer;
        in++;
    }

    *pwd_out = '\0';
    return final_errcheck(3);
}

int cmd_mkdir_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <dir_path>\n", argv[0]);
        return 2;
    }

    char pathbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(pathbuffer, argv[1]);

    sfs_mkdir(fs, pathbuffer);

    return final_errcheck(3);
}

int cmd_rmdir_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <dir_path>\n", argv[0]);
        return 2;
    }

    char pathbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(pathbuffer, argv[1]);

    sfs_rmdir(fs, pathbuffer);
    return final_errcheck(3);
}

int cmd_pwd_main(int argc, char **argv) {
    printf("%s\n", pwd);
    return 0;
}

int cmd_info_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <file_or_dir_path>\n", argv[0]);
        return 2;
    }

    char pathbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(pathbuffer, argv[1]);

    sfs_info(fs, pathbuffer);
    return final_errcheck(3);
}

int cmd_mv_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 3) {
        printf("Usage: %s <src> <dest>\n", argv[0]);
        return 2;
    }

    char srcbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(srcbuffer, argv[1]);

    char dstbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(dstbuffer, argv[2]);

    sfs_mv(fs, srcbuffer, dstbuffer);
    return final_errcheck(3);
}

int cmd_rm_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <file_path>\n", argv[0]);
        return 2;
    }

    char pathbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(pathbuffer, argv[1]);

    sfs_rm(fs, pathbuffer);
    return final_errcheck(3);
}

int cmd_cp_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 3) {
        printf("Usage: %s <src> <dest>\n", argv[0]);
        return 2;
    }

    char srcbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(srcbuffer, argv[1]);

    char dstbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(dstbuffer, argv[2]);

    sfs_stream *src = sfs_open(fs, srcbuffer, SFS_MODE_READ);
    if (!src) {
        return errcheck(3, 3);
    }

    sfs_stream *dst = sfs_open(fs, dstbuffer, SFS_MODE_WRITE);
    if (!dst) {
        sfs_close(src);
        return errcheck(3, 3);
    }

    int result = 0;
    while (!sfs_eof(src)) {
        char buffer[CP_BUFFER_SIZE];
        size_t read = sfs_read(buffer, sizeof(char), CP_BUFFER_SIZE, src);
        size_t written = sfs_write(buffer, sizeof(char), read, dst);

        if (read != written) {
            printf("Read/Write size mismatch!\n");
            result = 4;
            goto closing_end;
        }
    }

    closing_end:
    sfs_close(src);
    sfs_close(dst);
    return result;
}

int cmd_incp_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 3) {
        printf("Usage: %s <phys_src> <fs_dest>\n", argv[0]);
        return 2;
    }

    char *srcbuffer = argv[1];

    char dstbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(dstbuffer, argv[2]);

    FILE *src = fopen(srcbuffer, "rb");
    if (!src) {
        printf("FILE NOT FOUND\n");
        return 3;
    }

    sfs_stream *dst = sfs_open(fs, dstbuffer, SFS_MODE_WRITE);
    if (!dst) {
        fclose(src);
        return errcheck(3, 3);
    }

    int result = 0;
    while (!feof(src)) {
        char buffer[CP_BUFFER_SIZE];
        size_t read = fread(buffer, sizeof(char), CP_BUFFER_SIZE, src);
        size_t written = sfs_write(buffer, sizeof(char), read, dst);

        if (sfs_err()) {
            result = errcheck(0, 5);
            goto closing_end;
        }

        if (read != written) {
            printf("Read/Write size mismatch!\n");
            result = 4;
            goto closing_end;
        }
    }

    closing_end:
    fclose(src);
    sfs_close(dst);
    return result;
}

int cmd_outcp_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 3) {
        printf("Usage: %s <fs_src> <phys_dest>\n", argv[0]);
        return 2;
    }

    char srcbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(srcbuffer, argv[1]);

    char *dstbuffer = argv[2];

    sfs_stream *src = sfs_open(fs, srcbuffer, SFS_MODE_READ);
    if (!src) {
        return errcheck(3, 3);
    }

    FILE *dst = fopen(dstbuffer, "wb");
    if (!dst) {
        sfs_close(src);
        printf("PATH NOT FOUND\n");
        return 3;
    }

    int result = 0;
    while (!sfs_eof(src)) {
        char buffer[CP_BUFFER_SIZE];
        size_t read = sfs_read(buffer, sizeof(char), CP_BUFFER_SIZE, src);
        size_t written = fwrite(buffer, sizeof(char), read, dst);

        if (sfs_err()) {
            result = errcheck(0, 5);
            goto closing_end;
        }

        if (read != written) {
            printf("Read/Write size mismatch!\n");
            result = 4;
            goto closing_end;
        }
    }

    closing_end:
    sfs_close(src);
    fclose(dst);
    return result;
}

int cmd_cat_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    if (argc < 2) {
        printf("Usage: %s <file>\n", argv[0]);
        return 2;
    }

    char srcbuffer[MAX_PATH_LENGTH] = {0};
    pathcat(srcbuffer, argv[1]);

    sfs_stream *src = sfs_open(fs, srcbuffer, SFS_MODE_READ);
    if (!src) {
        return errcheck(3, 3);
    }

    int result = 0;
    while (!sfs_eof(src)) {
        char buffer[CP_BUFFER_SIZE];
        size_t read = sfs_read(buffer, sizeof(char), CP_BUFFER_SIZE, src);
        size_t written = fwrite(buffer, sizeof(char), read, stdout);

        if (sfs_err()) {
            result = errcheck(0, 5);
            goto closing_end;
        }

        if (read != written) {
            printf("\nRead/Write size mismatch!\n");
            result = 4;
            goto closing_end;
        }
    }


    closing_end:
    sfs_close(src);
    fflush(stdout);
    putchar('\n');
    return result;
}

int cmd_fsck_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    sfs_fsck(fs);
    return final_errcheck(2);
}

int cmd_break_orphan_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    sfs_fsckdbg_alloc_orphan(fs);
    return final_errcheck(2);
}

int cmd_break_iblock_main(int argc, char **argv) {
    if (ensure_fs()) {
        return 1;
    }

    sfs_fsckdbg_alloc_inadequate_blocks(fs);
    return final_errcheck(2);
}
