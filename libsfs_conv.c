
#include "liblowsfs_conv.h"
#include "libsfs_conv.h"

void htof_dir_entry(sfs_dir_entry *entry) {
    entry->inode_id = htof_inode_id(entry->inode_id);
}

void ftoh_dir_entry(sfs_dir_entry *entry) {
    entry->inode_id = ftoh_inode_id(entry->inode_id);
}
