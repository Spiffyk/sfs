#include <stdio.h>
#include <string.h>

#include "liblowsfs.h"
#include "libsfs.h"
#include "libsfscommon.h"

int main() {
    lsfs_vol *fs = lsfs_volume_open("../test/testfs.sfs");
    if (sfs_err()) {
        fprintf(stderr, "Open error: 0x%X\n", sfs_err());
        return 1;
    }

    printf("Size: %lu\n", fs->volume_size);
    printf("Valid before format: %d\n", fs->valid);

    sfs_format_params fp = sfs_format_params_gendefaults(fs);
    fp.clear = 1;
    fp.inode_count = 6;
    fp.block_size = 32;
    sfs_status fstatus = lsfs_format(fs, &fp);
    if (fstatus) {
        fprintf(stderr, "Format error: 0x%X\n", fstatus);
        return 1;
    }

    printf("Valid after format: %d\n", fs->valid);
    printf("Num. blocks: %lu\n", lsfs_header_get(fs)->block_count);
    printf("Used blocks on start: %u\n", lsfs_count_used_blocks(fs));
    printf("Block size: %u\n", lsfs_header_get(fs)->block_size);

    {
        char *test_src1 = "Jackdaws lo";
        char *test_src2 = "Hello, world!";
        char *test_src3 = "ve my big sphinx of quartz!";
        char *test_src4 = "Stonks";
        char test_dest[128] = {0};

        const lsfs_inode_id_t id_a = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_FILE);
        const lsfs_inode_id_t id_b = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_FILE);

        lsfs_stream *out1 = lsfs_open(fs, id_a, SFS_MODE_WRITE | SFS_MODE_APPEND);
        lsfs_write(test_src1, 1, strlen(test_src1), out1); /* excl \0 because we're gonna append more */
        lsfs_close(out1);

        lsfs_stream *out2 = lsfs_open(fs, id_b, SFS_MODE_WRITE | SFS_MODE_APPEND);
        lsfs_write(test_src2, 1, strlen(test_src2) + 1, out2); /* incl \0 */
        lsfs_close(out2);

        lsfs_stream *out3 = lsfs_open(fs, id_a, SFS_MODE_WRITE | SFS_MODE_APPEND);
        lsfs_write(test_src3, 1, strlen(test_src3) + 1, out3); /* incl \0 */
        lsfs_close(out3);

        lsfs_stream *in1 = lsfs_open(fs, id_a, SFS_MODE_READ);
        size_t read1 = lsfs_read(test_dest, 1, 128, in1);
        lsfs_close(in1);
        printf("Read A-1 (%lu): %s\n", read1, test_dest);

        lsfs_stream *in1_1 = lsfs_open(fs, id_b, SFS_MODE_READ);
        size_t read1_1 = lsfs_read(test_dest, 1, 128, in1_1);
        lsfs_close(in1_1);
        printf("Read B-1 (%lu): %s\n", read1_1, test_dest);

        printf("Used blocks after write: %u\n", lsfs_count_used_blocks(fs));

        lsfs_stream *out4 = lsfs_open(fs, id_a, SFS_MODE_WRITE);
        lsfs_write(test_src4, 1, strlen(test_src4) + 1, out4); /* incl \0 */
        lsfs_close(out4);

        lsfs_stream *in2 = lsfs_open(fs, id_a, SFS_MODE_READ);
        size_t read2 = lsfs_read(test_dest, 1, 128, in2);
        lsfs_close(in2);
        printf("Read A-2 (%lu): %s\n", read2, test_dest);

        printf("Used blocks after rewrite: %u\n", lsfs_count_used_blocks(fs));
        lsfs_inode_free(fs, id_a);
        printf("Used blocks after delete of inode A: %u\n", lsfs_count_used_blocks(fs));
        lsfs_inode_free(fs, id_b);
        printf("Used blocks after delete of inode B: %u\n", lsfs_count_used_blocks(fs));
    }

    lsfs_volume_close(&fs);
    if (sfs_err()) {
        fprintf(stderr, "Close error: 0x%X\n", sfs_err());
        return 1;
    }

    return 0;
}
