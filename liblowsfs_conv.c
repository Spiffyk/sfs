
#include "liblowsfs_fs_structs.h"

#include "liblowsfs_conv.h"

void htof_header(lsfs_header *header) {
    header->inode_count = htof_inode_id(header->inode_count);
    header->block_size = htof_block_size(header->block_size);
    header->block_count = htof_block_id(header->block_count);
}

void ftoh_header(lsfs_header *header) {
    header->inode_count = ftoh_inode_id(header->inode_count);
    header->block_size = ftoh_block_size(header->block_size);
    header->block_count = ftoh_block_id(header->block_count);
}

void htof_inode(lsfs_inode *inode) {
    inode->type = htof_inode_type(inode->type);
    inode->ref_count = htof_u32(inode->ref_count);
    inode->size = htof_file_size(inode->size);
    for (int i = 0; i < LSFS_INODE_NUM_DIRECTS; i++) {
        inode->blockrefs_d[i] = htof_block_id(inode->blockrefs_d[i]);
    }
    inode->blockrefs_i1 = htof_block_id(inode->blockrefs_i1);
    inode->blockrefs_i2 = htof_block_id(inode->blockrefs_i2);
}

void ftoh_inode(lsfs_inode *inode) {
    inode->type = ftoh_inode_type(inode->type);
    inode->ref_count = ftoh_u32(inode->ref_count);
    inode->size = ftoh_file_size(inode->size);
    for (int i = 0; i < LSFS_INODE_NUM_DIRECTS; i++) {
        inode->blockrefs_d[i] = ftoh_block_id(inode->blockrefs_d[i]);
    }
    inode->blockrefs_i1 = ftoh_block_id(inode->blockrefs_i1);
    inode->blockrefs_i2 = ftoh_block_id(inode->blockrefs_i2);
}

