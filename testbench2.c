#include <stdio.h>
#include <log.h>

#include "libsfs.h"
#include "libsfscommon.h"

void tb_ls(sfs *fs, const char *path);

int main() {
    log_set_level(LOG_DEBUG);

    sfs *fs = sfs_volume_open("../test/testfs.sfs");
    if (sfs_err()) {
        fprintf(stderr, "Open error: 0x%X\n", sfs_err());
        return 1;
    }

    sfs_format_params fp = sfs_format_params_gendefaults(fs);
    fp.clear = 1;
    fp.inode_count = 16;
    fp.block_size = 32;
    sfs_status fstatus = sfs_format(fs, &fp);
    if (fstatus) {
        fprintf(stderr, "Format error: 0x%04X\n", fstatus);
        return 1;
    }

    {
        sfs_mkdir(fs, "/test");
        sfs_mkdir(fs, "/test/boo");
        if (sfs_err()) {
            fprintf(stderr, "Test dirs error: 0x%04X\n", sfs_err());
            return 1;
        }
        sfs_mkdir(fs, "/ahoj");
        sfs_mkdir(fs, "bye");
        sfs_stream *s = sfs_open(fs, "/blub", SFS_MODE_WRITE);
        sfs_close(s);
        s = sfs_open(fs, "/test/c", SFS_MODE_WRITE);
        sfs_close(s);
        s = sfs_open(fs, "/ahoj/g", SFS_MODE_WRITE);
        sfs_close(s);

        tb_ls(fs, "/");
        tb_ls(fs, "/test");

        sfs_rm(fs, "/test/c");
        sfs_rmdir(fs, "/test/boo");
        sfs_mv(fs, "/ahoj", "/test/hojhoj");
        tb_ls(fs, "");
        tb_ls(fs, "./test/.");
        tb_ls(fs, "/test/hojhoj");
        tb_ls(fs, "/");

        sfs_info(fs, "/test");

        if (sfs_err()) {
            fprintf(stderr, "Test dirs error: 0x%04X\n", sfs_err());
            return 1;
        }
    }

    sfs_volume_close(&fs);
    if (sfs_err()) {
        fprintf(stderr, "Close error: 0x%04X\n", sfs_err());
        return 1;
    }

    return 0;
}

void tb_ls(sfs *fs, const char *path) {
    printf("\nListing '%s':\n", path);
    sfs_ls(fs, path);
    printf("\n\n");
}
