#ifndef SMORKFS_UTIL_MACROS_H
#define SMORKFS_UTIL_MACROS_H

#define cdiv(a, b) (((a) % (b)) ? (1 + ((a) / (b))) : ((a) / (b)))

#endif /* SMORKFS_UTIL_MACROS_H */
