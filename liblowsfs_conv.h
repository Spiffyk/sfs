/** @file
 * @brief Conversion utilities for the SFS low-level library.
 */

#ifndef SMORKFS_LIBLOWSFS_CONV_H
#define SMORKFS_LIBLOWSFS_CONV_H

#include <endian.h>
#include "liblowsfs.h"

#define htof_inode_id(i) htobe32(i) ///< Converts host input to filesystem output
#define ftoh_inode_id(i) be32toh(i) ///< Converts filesystem input to host output

#define htof_block_id(i) htobe64(i) ///< Converts host input to filesystem output
#define ftoh_block_id(i) be64toh(i) ///< Converts filesystem input to host output

#define htof_block_size(i) htobe32(i) ///< Converts host input to filesystem output
#define ftoh_block_size(i) be32toh(i) ///< Converts filesystem input to host output

#define htof_file_size(i) htobe64(i) ///< Converts host input to filesystem output
#define ftoh_file_size(i) be64toh(i) ///< Converts filesystem input to host output

#define htof_inode_type(i) (i) ///< Converts host input to filesystem output
#define ftoh_inode_type(i) (i) ///< Converts filesystem input to host output

#define htof_u32(i) htobe32(i) ///< Converts host input to filesystem output
#define ftoh_u32(i) be32toh(i) ///< Converts filesystem input to host output

void htof_header(lsfs_header *header); ///< Converts the host header at the specified pointer to filesystem header
void ftoh_header(lsfs_header *header); ///< Converts the filesystem header at the specified pointer to host header

void htof_inode(lsfs_inode *inode); ///< Converts the host inode at the specified pointer to filesystem inode
void ftoh_inode(lsfs_inode *inode); ///< Converts the filesystem inode at the specified pointer to host inode

#endif /* SMORKFS_LIBLOWSFS_CONV_H */
