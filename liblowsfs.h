/** @file
 * @brief The low-level SFS library.
 *
 * This library contains low-level functionality for interfacing with the filesystem.
 */

#ifndef SMORKFS_LIBLOWSFS_H
#define SMORKFS_LIBLOWSFS_H

#include "libsfscommon.h"
#include "liblowsfs_fs_structs.h"

/**
 * A structure for referencing of an SFS volume. This structure is opaque for the users of the high-level
 * library and should be referenced by the @ref sfs type there.
 */
typedef struct {
    FILE *file; ///< The file containing the whole volume
    uint64_t volume_size; ///< The size of the volume
    lsfs_header *header_view; ///< A pointer to the memory view of the volume header
    uint8_t valid; ///< Whether the volume is validly formatted
} lsfs_vol;

/**
 * A structure for referencing of an SFS file stream.
 */
typedef struct {
    sfs_stream_mode mode; ///< The mode of the stream.
    lsfs_vol *volume; ///< The volume this stream operates on
    lsfs_inode_id_t inode_id; ///< The ID of the inode this stream operates on
    lsfs_inode inode; ///< The host-formatted data of the inode this stream operates on
    long pos; ///< The current seek position of the stream
} lsfs_stream;

/**
 * Opens an SFS volume.
 *
 * @param [in] path
 *          The path to the volume containing the filesystem
 *
 * @return a reference to the SFS volume
 */
lsfs_vol * lsfs_volume_open(char *path);

/**
 * Closes an SFS volume and frees the resources associated with it.
 *
 * @param [out] target
 *          The volume to close
 */
void lsfs_volume_close(lsfs_vol **target);

/**
 * Checks if the SFS volume is valid.
 *
 * @param [in] fs
 *          The volume to check
 *
 * @return non-zero value if valid, zero if invalid
 */
int lsfs_volume_valid(lsfs_vol *fs);

/**
 * Formats the specified volume according to the specified parameters.
 *
 * @param [in] fs
 *          Reference to the volume to format
 * @param [in] params
 *          The formatting parameters
 *
 * @return current SFS status
 */
sfs_status lsfs_format(lsfs_vol *fs, sfs_format_params *params);

/**
 * Gets the view of the header of the specified volume.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [out] header
 *          Reference to a header to set to the volume's header
 *
 * @return current SFS status
 */
lsfs_header * lsfs_header_get(lsfs_vol *fs);

/**
 * Synchronizes the view of the header of the specified volume.
 *
 * @param [in] fs
 *          Reference to the volume
 *
 * @return current SFS status
 */
sfs_status lsfs_header_sync(lsfs_vol *fs);

/**
 * Reads inode metadata from the filesystem.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to read
 * @param [out] out
 *          The buffer to store the inode metadata into
 *
 * @return current SFS status
 */
sfs_status lsfs_inode_read(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode *out);

/**
 * Writes inode metadata into the filesystem.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to write
 * @param [in] in
 *          The buffer containing the inode metadata.
 *
 * @return current SFS status
 */
sfs_status lsfs_inode_write(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode *in);

/**
 * Initializes the inode with the specified ID with the specified type.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to initialize
 * @param [in] type
 *          The type of the inode
 */
void lsfs_inode_init(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode_type_t type);

/**
 * Finds a free inode and initializes it with the specified type.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] type
 *          The type of the inode to set
 *
 * @return the ID of the allocated inode or NULL if no free inode found
 */
lsfs_inode_id_t lsfs_inode_alloc(lsfs_vol *fs, lsfs_inode_type_t type);

/**
 * Frees the inode with the specified ID and all of its corresponding blocks.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to free
 *
 * @return current SFS status
 */
sfs_status lsfs_inode_free(lsfs_vol *fs, lsfs_inode_id_t id);

/**
 * Increments the reference counter of the specified inode.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to increment
 *
 * @return current SFS status
 */
sfs_status lsfs_inode_ref_inc(lsfs_vol *fs, lsfs_inode_id_t id);

/**
 * Decrements the reference counter of the specified inode and, if the counter reaches 0, frees the inode.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the inode to decrement
 *
 * @return current SFS status
 */
sfs_status lsfs_inode_ref_dec(lsfs_vol *fs, lsfs_inode_id_t id);

/**
 * Gets the `block_num`th block reference of the specified inode.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] inode
 *          The ID of the inode
 * @param [in] block_num
 *          The index of the block of the inode (starts at 0)
 *
 * @return the ID of the inode's `block_num`th block
 */
lsfs_block_id_t lsfs_inode_blockref_get(lsfs_vol *fs, lsfs_inode *inode, unsigned int block_num);

/**
 * Sets the `block_num`th block reference of the specified inode to the specified value.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] inode
 *          The ID of the inode
 * @param [in] block_num
 *          The index of the block of the inode (starts at 0)
 * @param [in] value
 *          The value to set
 */
void lsfs_inode_blockref_set(lsfs_vol *fs, lsfs_inode *inode, unsigned int block_num, lsfs_block_id_t value);

/**
 * Prints all block references of the specified inode into stdout.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] inode
 *          Pointer to an inode
 */
void lsfs_inode_print_blockrefs(lsfs_vol *fs, lsfs_inode *inode);

/**
 * Finds an unused block in the volume, marks it as used, and returns its ID.
 *
 * @param [in] fs
 *          Reference to the volume
 *
 * @return the ID of the block or @ref LSFS_BLOCK_NULL if no unused block found
 */
lsfs_block_id_t lsfs_block_alloc(lsfs_vol *fs);

/**
 * Finds an unused block in the volume, marks it as used, clears it to all 0, and returns its ID.
 *
 * @param [in] fs
 *          Reference to the volume
 *
 * @return the ID of the block or @ref LSFS_BLOCK_NULL if no unused block found
 */
lsfs_block_id_t lsfs_block_calloc(lsfs_vol *fs);

/**
 * Marks the specified block as unused.
 *
 * @param [in] fs
 *          Reference to the volume
 * @param [in] id
 *          The ID of the block to free
 *
 * @return current SFS status
 */
sfs_status lsfs_block_free(lsfs_vol *fs, lsfs_block_id_t id);

/**
 * Counts allocated blocks.
 *
 * @param [in] fs
 *          Reference to the volume
 *
 * @return the number of allocated blocks
 */
unsigned int lsfs_count_used_blocks(lsfs_vol *fs);

/**
 * Opens a stream on the inode with the specified ID. (ID `0` is the root inode)
 *
 * @param [in] fs
 *          The filesystem to open the stream in
 * @param [in] id
 *          The ID if the inode
 * @param [in] mode
 *          The read/write mode flags.
 *
 * @return a reference to a stream on success, @ref NULL otherwise
 */
lsfs_stream *lsfs_open(lsfs_vol *fs, lsfs_inode_id_t id, sfs_stream_mode mode);

/**
 * Flushes and closes the specified stream.
 *
 * @param [out] stream
 *          The stream to close
 */
void lsfs_close(lsfs_stream *stream);

/**
 * Moves the seek position of the specified stream.
 *
 * @param [out] stream
 *          The stream to seek in
 * @param [in] seek
 *          The amount of bytes to move by
 * @param [in] type
 *          The type of seek, may be @ref SFS_SEEK_CUR, @ref SFS_SEEK_SET or @ref SFS_SEEK_END
 */
void lsfs_seek(lsfs_stream *stream, long seek, sfs_seek_type type);

/**
 * Gets the current seek position of the specified stream.
 *
 * @param [in] stream
 *          The stream whose position is to be found
 *
 * @return the current seek position or `-1` if invalid
 */
long lsfs_tell(const lsfs_stream *stream);

/**
 * Checks whether the specified stream's seek position is at the end of the file.
 *
 * @param [in] stream
 *          The stream whose EOF status is to be checked
 *
 * @return `1` if the stream is at the end of the file, otherwise `0`
 */
int lsfs_eof(const lsfs_stream *stream);

/**
 * Discards everything on and after the current seek position of the writable stream.
 *
 * @param [out] stream
 *          The stream to write into
 *
 * @return the new size of the file
 */
long lsfs_trim(lsfs_stream *stream);

/**
 * Writes `elem_count` elements of `elem_size` bytes from `src` into the specified writable stream.
 *
 * @param [in] src
 *          The starting pointer of the data to be written
 * @param [in] elem_size
 *          The size of a single element
 * @param [in] elem_count
 *          The number of elements
 * @param [out] stream
 *          The stream to write into
 *
 * @return the number of successfully written elements
 */
size_t lsfs_write(const void* src, size_t elem_size, size_t elem_count, lsfs_stream *stream);

/**
 * Reads `elem_count` elements of `elem_size` bytes from the specified readable stream into `dest`.
 *
 * @param [out] dest
 *          The starting pointer of the buffer to put the read data in
 * @param [in] elem_size
 *          The size of a single element
 * @param [in] elem_count
 *          The number of elements
 * @param [out] stream
 *          The stream to read from
 *
 * @return the number of successfully read elements
 */
size_t lsfs_read(void* dest, size_t elem_size, size_t elem_count, lsfs_stream *stream);

#endif /*SMORKFS_LIBLOWSFS_H*/
