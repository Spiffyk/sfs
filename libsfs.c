#include <stdio.h>
#include <string.h>
#include <log.c>

#include "liblowsfs.h"
#include "liblowsfs_limits.h"
#include "libsfs.h"
#include "libsfs_conv.h"
#include "libsfs_fs_structs.h"

#include "util_macros.h"

void sfs_dir_entry_name_set(sfs_dir_entry *entry, const char *name);
sfs_dir_entry sfs_mkentry(lsfs_vol *fs, const char *name, lsfs_inode_id_t target, sfs_dir_entry_type_t type);
void sfs_unentry(lsfs_vol *fs, sfs_dir_entry *entry);
void sfs_mkdir_at_inode(lsfs_vol *fs, lsfs_inode_id_t target, lsfs_inode_id_t parent);
void sfs_ls_inode(lsfs_vol *fs, lsfs_inode_id_t dir);
sfs_stream *sfs_open_dir(lsfs_vol *fs, lsfs_inode_id_t dir_id, uint8_t mode);
void sfs_insert_entry(lsfs_vol *fs, lsfs_inode_id_t target, sfs_dir_entry *entry);
unsigned char sfs_remove_entry(lsfs_vol *fs, lsfs_inode_id_t target, const char *name, unsigned char allow_system);
lsfs_inode_id_t sfs_resolve_inode_r(lsfs_vol *fs, const char *path, const char* limit);
lsfs_inode_id_t sfs_resolve_inode_from_parent(lsfs_vol *fs, lsfs_inode_id_t parent, const char *name);

const char *last_element_of(const char *string, const char *limit);
const char *last_char_of(const char *string);
unsigned char read_entry_name_from_path(char *dest, const char *start, const char *limit);

void fsck_orphaned_inodes(sfs *fs);
void fsck_orphaned_inodes_crawl(sfs *fs, char *markers, lsfs_inode_id_t id);
void fsck_inadequate_blocks(sfs *fs);

sfs_format_params sfs_format_params_gendefaults(sfs *fs) { // NOLINT
    sfs_format_params result;
    result.inode_count = 1024; // TODO calculate this from the size of the volume
    result.block_size = 1024 * 1024;
    result.clear = 0;
    return result;
}

sfs *sfs_volume_open(char *path) {
    log_trace("Opening volume at '%s'...", path);
    return (sfs *) lsfs_volume_open(path);
}

void sfs_volume_close(sfs **fs) {
    lsfs_volume_close((lsfs_vol **) fs);
}

int sfs_volume_valid(sfs *fs) {
    return lsfs_volume_valid(fs);
}

sfs_status sfs_format(sfs *fs, sfs_format_params *params) {
    lsfs_format((lsfs_vol *) fs, params);
    sfs_mkdir_at_inode(fs, LSFS_INODE_ROOT, LSFS_INODE_ROOT);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

int sfs_isdir(sfs *fs, const char *path) {
    lsfs_inode_id_t inode_id = sfs_resolve_inode_r(fs, path, last_char_of(path));
    if (!inode_id) {
        return 0;
    }

    lsfs_inode inode;
    lsfs_inode_read(fs, inode_id, &inode);
    if (sfs_err()) {
        log_error("Could not read inode %04x!", inode_id);
        return 0;
    }

    return (inode.type == LSFS_INODE_TYPE_DIRECTORY);
}

sfs_status sfs_mkdir(sfs *fs, const char *path) {
    log_trace("Creating directory at '%s'...", path);

    const char *limit = last_char_of(path);
    const char *name_start = last_element_of(path, limit);
    lsfs_inode_id_t parent = sfs_resolve_inode_r(fs, path, name_start);
    if (!parent) {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            sfs_seterr(SFS_STATUS_ERR_NPATH);
        }
        return sfs_err();
    }

    char dir_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(dir_name, name_start, limit);
    if (!name_success) {
        return sfs_err();
    }

    lsfs_inode_id_t dir_id = sfs_resolve_inode_from_parent(fs, parent, dir_name);
    if (dir_id) {
        lsfs_inode inode;
        sfs_status err = lsfs_inode_read(fs, dir_id, &inode);
        if (err) {
            return sfs_err();
        }

        if (inode.type != LSFS_INODE_TYPE_DIRECTORY) {
            log_error("The entry '%s' exists but is not a directory!", path);
            // the entry exists but is not a directory, this is an error
            return sfs_seterr(SFS_STATUS_ERR_EXISTS);
        }

        // directory already exists, we're done here
        log_debug("The directory '%s' already exists, skipping", path);
        return sfs_err();
    }

    if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
        log_debug("The directory '%s' does not exist, creating...", path);
        sfs_reseterr();
    }

    dir_id = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_DIRECTORY);
    if (!dir_id) {
        return sfs_err();
    }

    sfs_mkdir_at_inode(fs, dir_id, parent);
    sfs_dir_entry entry = sfs_mkentry(fs, dir_name, dir_id, SFS_DIR_ENTRY_TYPE_REGULAR);
    sfs_insert_entry(fs, parent, &entry);

    log_debug("Created directory '%s'", path);
    return sfs_err();
}

sfs_status sfs_rmdir(sfs *fs, const char *path) {
    const char *limit = last_char_of(path);
    const char *name_start = last_element_of(path, limit);
    lsfs_inode_id_t parent = sfs_resolve_inode_r(fs, path, name_start);
    if (!parent) {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            sfs_seterr(SFS_STATUS_ERR_NPATH);
        }
        return sfs_err();
    }

    char removed_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(removed_name, name_start, limit);
    if (!name_success) {
        return sfs_err();
    }

    lsfs_inode_id_t removed_id = sfs_resolve_inode_from_parent(fs, parent, removed_name);
    if (!removed_id) {
        return sfs_err();
    }

    // check for non-system entries
    lsfs_stream *stream = sfs_open_dir(fs, removed_id, SFS_MODE_READ);
    if (!stream) {
        return sfs_err();
    }
    while (!lsfs_eof(stream)) {
        sfs_dir_entry entry;
        lsfs_read(&entry, sizeof(sfs_dir_entry), 1, stream);
        ftoh_dir_entry(&entry);

        if (entry.type != SFS_DIR_ENTRY_TYPE_SYSTEM) {
            lsfs_close(stream);
            return sfs_seterr(SFS_STATUS_ERR_NEMPTY);
        }
    }
    lsfs_close(stream);

    sfs_remove_entry(fs, parent, removed_name, 0);
    if (sfs_err()) {
        log_error("Could not remove directory '%s' from parent %04x", removed_name, parent);
        return sfs_err();
    }

    lsfs_inode removed_inode;
    lsfs_inode_read(fs, removed_id, &removed_inode);

    sfs_remove_entry(fs, removed_id, "..", 1);
    sfs_remove_entry(fs, removed_id, ".", 1);
    return sfs_err();
}

sfs_status sfs_rm(sfs *fs, const char *path) {
    const char *limit = last_char_of(path);
    const char *name_start = last_element_of(path, limit);
    lsfs_inode_id_t parent = sfs_resolve_inode_r(fs, path, name_start);
    if (!parent) {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            sfs_seterr(SFS_STATUS_ERR_NPATH);
        }
        return sfs_err();
    }

    char removed_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(removed_name, name_start, limit);
    if (!name_success) {
        return sfs_err();
    }

    lsfs_inode_id_t removed_id = sfs_resolve_inode_from_parent(fs, parent, removed_name);
    if (!removed_id) {
        return sfs_err();
    }

    lsfs_inode inode;
    sfs_status err = lsfs_inode_read(fs, removed_id, &inode);
    if (err) {
        return sfs_err();
    }

    if (inode.type != LSFS_INODE_TYPE_FILE) {
        return sfs_seterr(SFS_STATUS_ERR_NFILE);
    }

    sfs_remove_entry(fs, parent, removed_name, 0);
    return sfs_err();
}

sfs_status sfs_mv(sfs *fs, const char *srcpath, const char *dstpath) {
    // Get source and check if it exists
    const char *src_limit = last_char_of(srcpath);
    const char *src_name_start = last_element_of(srcpath, src_limit);
    lsfs_inode_id_t src_parent = sfs_resolve_inode_r(fs, srcpath, src_name_start);
    if (!src_parent) {
        return sfs_err();
    }

    char src_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char src_name_success = read_entry_name_from_path(src_name, src_name_start, src_limit);
    if (!src_name_success) {
        return sfs_err();
    }

    lsfs_inode_id_t src_inode = sfs_resolve_inode_from_parent(fs, src_parent, src_name);
    if (!src_inode) {
        return sfs_err();
    }

    // Get destination and check if its parent is a directory
    const char *dst_limit = last_char_of(dstpath);
    const char *dst_name_start = last_element_of(dstpath, dst_limit);
    lsfs_inode_id_t dst_parent = sfs_resolve_inode_r(fs, dstpath, dst_name_start);
    if (!dst_parent) {
        return sfs_err();
    }

    char dst_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char dst_name_success = read_entry_name_from_path(dst_name, dst_name_start, dst_limit);
    if (!dst_name_success) {
        return sfs_err();
    }

    sfs_dir_entry dst_entry = sfs_mkentry(fs, dst_name, src_inode, SFS_DIR_ENTRY_TYPE_REGULAR);
    sfs_insert_entry(fs, dst_parent, &dst_entry);
    if (sfs_err()) {
        sfs_unentry(fs, &dst_entry);
        return sfs_err();
    }

    sfs_remove_entry(fs, src_parent, src_name, 0);
    return sfs_err();
}

sfs_status sfs_ls(sfs *fs, const char *path) {
    lsfs_inode_id_t inode = sfs_resolve_inode_r(fs, path, last_char_of(path));
    if (!inode) {
        return sfs_err();
    }

    sfs_ls_inode(fs, inode);

    return sfs_err();
}

sfs_status sfs_info(sfs *fs, const char *path) {
    const char *limit = last_char_of(path);
    const char *name_start = last_element_of(path, limit);
    lsfs_inode_id_t parent = sfs_resolve_inode_r(fs, path, name_start);
    if (!parent) {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            sfs_seterr(SFS_STATUS_ERR_NPATH);
        }
        return sfs_err();
    }

    char name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(name, name_start, limit);
    if (!name_success) {
        return sfs_err();
    }

    lsfs_inode_id_t inode_id = sfs_resolve_inode_from_parent(fs, parent, name);
    if (!inode_id) {
        return sfs_err();
    }

    lsfs_inode inode;
    lsfs_inode_read(fs, inode_id, &inode);

    printf("%s - %lu - i-node %04x - ", name, inode.size, inode_id);
    lsfs_inode_print_blockrefs(fs, &inode);
    putchar('\n');

    return sfs_err();
}

sfs_stream *sfs_open(sfs *fs, const char *path, sfs_stream_mode mode) {
    const char *limit = last_char_of(path);
    const char *name_start = last_element_of(path, limit);
    lsfs_inode_id_t parent = sfs_resolve_inode_r(fs, path, name_start);
    if (!parent) {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            sfs_seterr(SFS_STATUS_ERR_NPATH);
        }
        return NULL;
    }

    char opened_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(opened_name, name_start, limit);
    if (!name_success) {
        return NULL;
    }

    lsfs_inode_id_t opened_id = sfs_resolve_inode_from_parent(fs, parent, opened_name);
    if (opened_id) {
        lsfs_inode inode;
        sfs_status err = lsfs_inode_read(fs, opened_id, &inode);
        if (err) {
            return NULL;
        }

        if (inode.type != LSFS_INODE_TYPE_FILE) {
            sfs_seterr(SFS_STATUS_ERR_NFILE);
            return NULL;
        }
    } else {
        if (sfs_err() == SFS_STATUS_ERR_NEXISTS) {
            sfs_reseterr();
            opened_id = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_FILE);
            if (!opened_id) {
                return NULL;
            }

            sfs_dir_entry opened_entry = sfs_mkentry(fs, opened_name, opened_id, SFS_DIR_ENTRY_TYPE_REGULAR);
            sfs_insert_entry(fs, parent, &opened_entry);
        } else {
            return NULL;
        }
    }

    return lsfs_open(fs, opened_id, mode);
}

void sfs_close(sfs_stream *stream) {
    lsfs_close(stream);
}

void sfs_seek(sfs_stream *stream, long seek, sfs_seek_type type) {
    lsfs_seek(stream, seek, type);
}

long sfs_tell(const sfs_stream *stream) {
    return lsfs_tell(stream);
}

int sfs_eof(const sfs_stream *stream) {
    return lsfs_eof(stream);
}

long sfs_trim(sfs_stream *stream) {
    return lsfs_trim(stream);
}

size_t sfs_write(const void* src, size_t elem_size, size_t elem_count, sfs_stream *stream) {
    return lsfs_write(src, elem_size, elem_count, stream);
}

size_t sfs_read(void* dest, size_t elem_size, size_t elem_count, sfs_stream *stream) {
    return lsfs_read(dest, elem_size, elem_count, stream);
}


void sfs_dir_entry_name_set(sfs_dir_entry *entry, const char *name) {
    unsigned int i;
    for (i = 0; i < SFS_DIR_ENTRY_NAME_LENGTH; i++) {
        entry->name[i] = (uint8_t) name[i];
        if (!name[i]) {
            break;
        }
    }
    for (; i < SFS_DIR_ENTRY_NAME_LENGTH; i++) {
        entry->name[i] = '\0';
    }
}

sfs_dir_entry sfs_mkentry(lsfs_vol *fs, const char *name, lsfs_inode_id_t target, sfs_dir_entry_type_t type) {
    sfs_dir_entry entry;
    sfs_dir_entry_name_set(&entry, name);
    entry.inode_id = target;
    entry.type = type;

    lsfs_inode_ref_inc(fs, target);

    return entry;
}

void sfs_unentry(lsfs_vol *fs, sfs_dir_entry *entry) {
    lsfs_inode_ref_dec(fs, entry->inode_id);
}

void sfs_mkdir_at_inode(lsfs_vol *fs, lsfs_inode_id_t target, lsfs_inode_id_t parent) {
    lsfs_inode_init(fs, target, LSFS_INODE_TYPE_DIRECTORY);

    lsfs_stream *out = lsfs_open(fs, target, SFS_MODE_WRITE);
    if (!out) {
        return;
    }
    sfs_dir_magic_t magic = SFS_DIR_MAGIC_V1;
    lsfs_write(&magic, sizeof(sfs_dir_magic_t), 1, out);
    lsfs_close(out);

    sfs_dir_entry parent_ref = sfs_mkentry(fs, "..", parent, SFS_DIR_ENTRY_TYPE_SYSTEM);
    sfs_insert_entry(fs, target, &parent_ref);

    sfs_dir_entry self_ref = sfs_mkentry(fs, ".", target, SFS_DIR_ENTRY_TYPE_SYSTEM);
    sfs_insert_entry(fs, target, &self_ref);
}

void sfs_ls_inode(lsfs_vol *fs, lsfs_inode_id_t dir) {
    lsfs_stream *stream = sfs_open_dir(fs, dir, SFS_MODE_READ);
    if (!stream) {
        return;
    }

    unsigned long count = 0;

    while (!lsfs_eof(stream)) {
        sfs_dir_entry entry;
        lsfs_read(&entry, sizeof(sfs_dir_entry), 1, stream);
        ftoh_dir_entry(&entry);

        lsfs_inode inode;
        lsfs_inode_read(fs, entry.inode_id, &inode);
        if (sfs_err()) {
            goto closing_end;
        }
        char typechar;
        switch (inode.type) {
            case LSFS_INODE_TYPE_DIRECTORY:
                typechar = '+';
                break;
            case LSFS_INODE_TYPE_FILE:
                typechar = '-';
                break;
            default:
                typechar = '?';
        }

        printf("%c %s\n", typechar, entry.name);
        count++;
    }

    closing_end:
    lsfs_close(stream);
}

sfs_stream *sfs_open_dir(lsfs_vol *fs, lsfs_inode_id_t dir_id, uint8_t mode) {
    lsfs_inode inode;
    lsfs_inode_read(fs, dir_id, &inode);
    if (inode.type != LSFS_INODE_TYPE_DIRECTORY) {
        sfs_seterr(SFS_STATUS_ERR_NDIR);
        return NULL;
    }

    lsfs_stream *stream = lsfs_open(fs, dir_id, mode | SFS_MODE_READ | SFS_MODE_NOREPLACE);
    if (!stream) {
        sfs_seterr(SFS_STATUS_ERR_IO);
        return NULL;
    }

    sfs_dir_magic_t magic = 0;
    lsfs_read(&magic, sizeof(sfs_dir_magic_t), 1, stream);
    if (magic != SFS_DIR_MAGIC_V1) {
        sfs_seterr(SFS_STATUS_ERR_NDIR);
        lsfs_close(stream);
        return NULL;
    }

    return stream;
}

void sfs_insert_entry(lsfs_vol *fs, lsfs_inode_id_t target, sfs_dir_entry *entry) {
    sfs_dir_entry inserted = *entry;
    htof_dir_entry(&inserted);

    lsfs_stream *stream = sfs_open_dir(fs, target, SFS_MODE_WRITE);
    if (!stream) {
        sfs_seterr(SFS_STATUS_ERR_IO);
        return;
    }

    if (stream->inode.size == sizeof(sfs_dir_magic_t)) {
        lsfs_seek(stream, 0, SFS_SEEK_END);
        lsfs_write(&inserted, sizeof(sfs_dir_entry), 1, stream);
        goto closing_end;
    }

    const long entry_size = (long) sizeof(sfs_dir_entry);
    lsfs_seek(stream, -entry_size, SFS_SEEK_END);

    for(;;) {
        if (lsfs_tell(stream) < sizeof(sfs_dir_magic_t) || lsfs_eof(stream)) {
            lsfs_seek(stream, sizeof(sfs_dir_magic_t), SFS_SEEK_SET);
            lsfs_write(&inserted, sizeof(sfs_dir_entry), 1, stream);
            goto closing_end;
        }

        sfs_dir_entry existing;
        lsfs_read(&existing, sizeof(sfs_dir_entry), 1, stream);

        int cmp = strncmp((char *) inserted.name, (char *) existing.name, SFS_DIR_ENTRY_NAME_LENGTH);
        if (cmp > 0) {
            // place inserted
            lsfs_write(&inserted, sizeof(sfs_dir_entry), 1, stream);
            goto closing_end;
        } else if (cmp < 0) {
            // nudge existing
            lsfs_write(&existing, sizeof(sfs_dir_entry), 1, stream);
            lsfs_seek(stream, (-3 * entry_size), SFS_SEEK_CUR);
            continue;
        } else /* cmp == 0 */ {
            log_error("Entry '%s' already exists in parent %04x", entry->name, target);
            sfs_seterr(SFS_STATUS_ERR_EXISTS);
            goto closing_end;
        }
    }

    closing_end:
    lsfs_close(stream);
}

unsigned char sfs_remove_entry(lsfs_vol *fs, lsfs_inode_id_t target, const char *name, unsigned char allow_system) {
    const long entry_size = (long) sizeof(sfs_dir_entry);

    size_t name_len = strlen(name);
    if (name_len > SFS_DIR_ENTRY_NAME_LENGTH || name_len == 0) {
        return 0;
    }

    lsfs_stream *stream = sfs_open_dir(fs, target, SFS_MODE_WRITE);
    if (!stream) {
        sfs_seterr(SFS_STATUS_ERR_IO);
        return 0;
    }

    // find the entry to remove
    unsigned char result = 0;
    for(;;) {
        if (lsfs_eof(stream)) {
            goto closing_end;
        }

        sfs_dir_entry checked;
        lsfs_read(&checked, sizeof(sfs_dir_entry), 1, stream);
        ftoh_dir_entry(&checked);

        int cmp = strncmp(name, (char *) checked.name, SFS_DIR_ENTRY_NAME_LENGTH);
        if (cmp == 0) {
            if (checked.type == SFS_DIR_ENTRY_TYPE_SYSTEM && !allow_system) {
                sfs_seterr(SFS_STATUS_ERR_REMSYSTEM);
                goto closing_end;
            }
            long pos = lsfs_tell(stream);
            lsfs_close(stream);
            sfs_unentry(fs, &checked);
            stream = sfs_open_dir(fs, target, SFS_MODE_WRITE);
            if (!stream) {
                if (sfs_err() == SFS_STATUS_ERR_NDIR) {
                    sfs_reseterr();
                    return 1;
                } else {
                    return 0;
                }
            }
            lsfs_seek(stream, pos, SFS_SEEK_SET);
            goto nudge_further;
        } else if (cmp < 0) {
            goto closing_end;
        }
    }

    // nudge all entries to the left
    nudge_further:
    result = 1;
    while(!lsfs_eof(stream)) {
        sfs_dir_entry nudged;
        lsfs_read(&nudged, sizeof(sfs_dir_entry), 1, stream);
        lsfs_seek(stream, (-2 * entry_size), SFS_SEEK_CUR);
        lsfs_write(&nudged, sizeof(sfs_dir_entry), 1, stream);
        lsfs_seek(stream, entry_size, SFS_SEEK_CUR);
    }

    lsfs_seek(stream, -entry_size, SFS_SEEK_CUR);
    lsfs_trim(stream);

    closing_end:
    lsfs_close(stream);
    return result;
}

lsfs_inode_id_t sfs_resolve_inode_r(lsfs_vol *fs, const char *path, const char* limit) {
    if (limit < path || (limit == path && *path == '/')) {
        return LSFS_INODE_ROOT;
    }

    // Seek to current entry name start
    const char *new_limit = last_element_of(path, limit);

    // Resolve current entry name
    char searched_name[SFS_DIR_ENTRY_NAME_LENGTH + 1] = {0};
    unsigned char name_success = read_entry_name_from_path(searched_name, new_limit, limit);
    if (!name_success) {
        return LSFS_INODE_NULL;
    }

    // Resolve parent inode
    new_limit--;
    lsfs_inode_id_t parent_inode_id = sfs_resolve_inode_r(fs, path, new_limit);
    return sfs_resolve_inode_from_parent(fs, parent_inode_id, searched_name);
}

lsfs_inode_id_t sfs_resolve_inode_from_parent(lsfs_vol *fs, lsfs_inode_id_t parent, const char *name) {
    if (!parent) {
        return LSFS_INODE_NULL;
    }

    if (strlen(name) == 0) {
        return parent;
    }

    lsfs_inode_id_t result = LSFS_INODE_NULL;
    lsfs_stream *dir = sfs_open_dir(fs, parent, SFS_MODE_READ);
    if (!dir) {
        return LSFS_INODE_NULL;
    }
    while (!lsfs_eof(dir)) {
        sfs_dir_entry entry;
        lsfs_read(&entry, sizeof(sfs_dir_entry), 1, dir);
        ftoh_dir_entry(&entry);

        const int cmp = strncmp(name, (char *) entry.name, SFS_DIR_ENTRY_NAME_LENGTH);
        if (cmp == 0) {
            result = entry.inode_id;
            break;
        } else if (cmp < 0) {
            // No sense searching for any 'greater' entry in an alphabetically-ordered list
            break;
        }
    }

    lsfs_close(dir);
    if (!result) {
        log_warn("Could not resolve entry '%s' from parent %04x", name, parent);
        sfs_seterr(SFS_STATUS_ERR_NEXISTS);
    }
    if (sfs_err()) {
        return LSFS_INODE_NULL;
    }
    return result;
}

const char *last_element_of(const char *string, const char *limit) {
    if (*limit == '/') {
        limit--;
    }

    while (limit > string && *limit != '/') {
        limit--;
    }
    return limit;
}

const char *last_char_of(const char *string) {
    while (*string) {
        string++;
    }
    string--;
    return string;
}

unsigned char read_entry_name_from_path(char *dest, const char *start, const char *limit) {
    unsigned int ename_len = 0;
    char *ename_write_p = dest;
    const char *ename_read_p = start;
    if (*ename_read_p == '/') {
        ename_read_p++;
    }
    while (*ename_read_p != '/' && ename_read_p <= limit) {
        if (ename_len >= SFS_DIR_ENTRY_NAME_LENGTH) {
            sfs_seterr(SFS_STATUS_ERR_LONGNAME);
            return 0;
        }

        *ename_write_p = *ename_read_p;

        ename_len++;
        ename_write_p++;
        ename_read_p++;
    }

    return 1;
}

void sfs_fsck(sfs *fs) {
    fsck_orphaned_inodes(fs);
    fsck_inadequate_blocks(fs);
}

void fsck_orphaned_inodes(sfs *fs) {
    printf("=== Scanning for orphaned inodes... ===\n");

    const lsfs_inode_id_t inode_count = lsfs_header_get(fs)->inode_count;
    char *markers = calloc(inode_count, sizeof(char));
    fsck_orphaned_inodes_crawl(fs, markers, LSFS_INODE_ROOT);

    lsfs_inode_id_t orphan_count = 0;
    for (lsfs_inode_id_t i = 1; i <= inode_count; i++) {
        lsfs_inode inode;
        lsfs_inode_read(fs, i, &inode);

        if (inode.type && !markers[i - 1]) {
            printf("Found orphaned inode %04x\n", i);
            orphan_count++;
        }
    }

    free(markers);
    printf("=== Scan for orphaned inodes complete - %u total orphaned inodes ===\n\n", orphan_count);
}

void fsck_orphaned_inodes_crawl(sfs *fs, char *markers, lsfs_inode_id_t id) {
    if (markers[id - 1]) {
        return;
    }

    markers[id - 1] = 1;

    lsfs_inode inode;
    lsfs_inode_read(fs, id, &inode);

    if (inode.type == LSFS_INODE_TYPE_DIRECTORY) {
        lsfs_stream *dir = sfs_open_dir(fs, id, SFS_MODE_READ);
        while (!lsfs_eof(dir)) {
            sfs_dir_entry entry;
            lsfs_read(&entry, sizeof(sfs_dir_entry), 1, dir);
            ftoh_dir_entry(&entry);

            if (entry.type == SFS_DIR_ENTRY_TYPE_REGULAR) {
                fsck_orphaned_inodes_crawl(fs, markers, entry.inode_id);
            }
        }
        sfs_close(dir);
    }
}

void fsck_inadequate_blocks(sfs *fs) {
    printf("=== Scanning for inodes with inadequate blocks... ===\n");
    const lsfs_header *header = lsfs_header_get(fs);
    const lsfs_inode_id_t inode_count = header->inode_count;
    const uint32_t block_size = header->block_size;

    const unsigned int max_blocks = lsfs_inode_max_blocks(fs);
    lsfs_inode_id_t too_many_blocks = 0;
    for (lsfs_inode_id_t i = 1; i <= inode_count; i++) {
        lsfs_inode inode;
        lsfs_inode_read(fs, i, &inode);

        if (!inode.type) {
            continue;
        }

        const unsigned int expected_blocks = (unsigned int) cdiv(inode.size, block_size);
        unsigned int used_blocks = 0;
        for (unsigned int j = 0; j < max_blocks; j++) {
            lsfs_block_id_t block_id = lsfs_inode_blockref_get(fs, &inode, j);
            if (block_id) {
                used_blocks++;
            }
        }

        if (used_blocks != expected_blocks) {
            printf("Found inode %04x with %u referenced blocks but %u were calculated by size\n", i, used_blocks, expected_blocks);
        }
    }
    printf("=== Scan for inodes with inadequate blocks complete - %u total inodes with inadequate blocks ===\n\n", too_many_blocks);
}

void sfs_fsckdbg_alloc_orphan(sfs *fs) {
    lsfs_inode_id_t id = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_FILE);
    printf("Allocated orphaned inode %04x\n", id);
}

void sfs_fsckdbg_alloc_inadequate_blocks(sfs *fs) {
    lsfs_inode_id_t id = lsfs_inode_alloc(fs, LSFS_INODE_TYPE_FILE);
    lsfs_inode inode;
    lsfs_inode_read(fs, id, &inode);
    for (unsigned int i = 0; i < 3; i++) {
        lsfs_inode_blockref_set(fs, &inode, i, lsfs_block_calloc(fs));
    }
    lsfs_inode_write(fs, id, &inode);
    printf("Allocated inode %04x with an inadequate number of blocks (it is also orphaned)\n", id);
}
