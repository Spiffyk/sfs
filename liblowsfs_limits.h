/** @file
 * @brief Utilities for determining starting/ending points of individual structures for the SFS low-level library.
 */

#ifndef SMORKFS_LIBLOWSFS_LIMITS_H
#define SMORKFS_LIBLOWSFS_LIMITS_H

#include "liblowsfs.h"

/**
 * Starting byte of the filesystem header.
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the starting byte
 */
static inline long lsfs_header_start_h(lsfs_header *h) {
    return 0;
}

/**
 * Starting byte of inodes (the first inode).
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the starting byte
 */
static inline long lsfs_inodes_start_h(lsfs_header *h) {
    return (long) sizeof(lsfs_header);
}

/**
 * Starting byte of the inode with the specified ID.
 *
 * @param [in] h pointer to a host-formatted header
 * @param [in] id ID of the inode
 *
 * @return the starting byte
 */
static inline long lsfs_inode_start_h(lsfs_header *h, lsfs_inode_id_t id) {
    return lsfs_inodes_start_h(h) + (long) sizeof(lsfs_inode) * (long) (id - 1);
}

/**
 * Starting byte of the bitmap.
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the starting byte
 */
static inline long lsfs_bitmap_start_h(lsfs_header *h) {
    return lsfs_inode_start_h(h, h->inode_count + 1);
}

/**
 * Number of bytes in the bitmap.
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the number of bytes
 */
static inline size_t lsfs_bitmap_size_h(lsfs_header *h) {
    unsigned long bytes = h->block_count / 8;
    return (h->block_count % 8 > 0)
           ? (bytes + 1)
           : (bytes);
}

/**
 * Starting byte of blocks (the first block).
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the starting byte
 */
static inline long lsfs_blocks_start_h(lsfs_header *h) {
    return lsfs_bitmap_start_h(h) + (long) lsfs_bitmap_size_h(h);
}

/**
 * Starting byte of the block with the specified ID.
 *
 * @param [in] h pointer to a host-formatted header
 * @param [in] id ID of the block
 *
 * @return the starting byte
 */
static inline long lsfs_block_start_h(lsfs_header *h, lsfs_block_id_t id) {
    return lsfs_blocks_start_h(h) + ((long) (h->block_size) * (long) (id - 1));
}

/**
 * The total size of the filesystem in bytes.
 *
 * @param [in] h pointer to a host-formatted header
 *
 * @return the total size
 */
static inline long lsfs_total_size_h(lsfs_header *h) {
    return lsfs_block_start_h(h, h->block_count + 1);
}

/**
 * Starting byte of the filesystem header.
 *
 * @param [in] fs pointer to a volume
 *
 * @return the starting byte
 */
static inline long lsfs_header_start(lsfs_vol *fs) {
    return 0;
}

/**
 * Starting byte of inodes (the first inode).
 *
 * @param [in] fs pointer to a volume
 *
 * @return the starting byte
 */
static inline long lsfs_inodes_start(lsfs_vol *fs) {
    return lsfs_inodes_start_h(lsfs_header_get(fs));
}

/**
 * Starting byte of the inode with the specified ID.
 *
 * @param [in] fs pointer to a volume
 * @param [in] id ID of the inode
 *
 * @return the starting byte
 */
static inline long lsfs_inode_start(lsfs_vol *fs, lsfs_inode_id_t id) {
    return lsfs_inode_start_h(lsfs_header_get(fs), id);
}

/**
 * Starting byte of the bitmap.
 *
 * @param [in] fs pointer to a volume
 *
 * @return the starting byte
 */
static inline long lsfs_bitmap_start(lsfs_vol *fs) {
    return lsfs_bitmap_start_h(lsfs_header_get(fs));
}

/**
 * Number of bytes in the bitmap.
 *
 * @param [in] fs pointer to a volume
 *
 * @return the starting byte
 */
static inline long lsfs_bitmap_size(lsfs_vol *fs) {
    return lsfs_bitmap_size_h(lsfs_header_get(fs));
}

/**
 * Starting byte of blocks (the first block).
 *
 * @param [in] fs pointer to a volume
 *
 * @return the starting byte
 */
static inline long lsfs_blocks_start(lsfs_vol *fs) {
    return lsfs_blocks_start_h(lsfs_header_get(fs));
}

/**
 * Starting byte of the block with the specified ID.
 *
 * @param [in] fs pointer to a volume
 * @param [in] id ID of the block
 *
 * @return the starting byte
 */
static inline long lsfs_block_start(lsfs_vol *fs, lsfs_block_id_t id) {
    return lsfs_block_start_h(lsfs_header_get(fs), id);
}

/**
 * The total size of the filesystem in bytes.
 *
 * @param [in] fs
 *          pointer to a volume
 *
 * @return the total size
 */
static inline long lsfs_total_size(lsfs_vol *fs) {
    return lsfs_total_size_h(lsfs_header_get(fs));
}

/**
 * Gets the number of indirect data block references contained in a single block.
 *
 * @param [in] fs
 *          pointer to a volume
 *
 * @return number of references
 */
static inline unsigned int lsfs_num_indirects(lsfs_vol *fs) {
    return lsfs_header_get(fs)->block_size / sizeof(lsfs_block_id_t);
}

/**
 * Gets the maximum number of blocks an inode can have.
 *
 * @param [in] fs
 *          pointer to a volume
 *
 * @return maximum number of blocks
 */
static inline unsigned int lsfs_inode_max_blocks(lsfs_vol *fs) {
    unsigned int num_i = lsfs_num_indirects(fs);
    return LSFS_INODE_NUM_DIRECTS + num_i * (num_i + 1);
}

#endif /* SMORKFS_LIBLOWSFS_LIMITS_H */
