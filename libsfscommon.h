/** @file
 * @brief Common functionality of all the SFS libraries.
 */

#ifndef SMORKFS_LIBSFSCOMMON_H
#define SMORKFS_LIBSFSCOMMON_H

#include <stdio.h>
#include <stdint.h>

/**
 * A data type for status codes returned by SFS functions.
 */
typedef unsigned int sfs_status;

#define SFS_STATUS_SUCCESS ((sfs_status) 0x0000) ///< A status code returned by a successful operation, always guaranteed to be zero for convenient checking

#define SFS_STATUS_ERR_MEMORY ((sfs_status) 0x0001) ///< A status code for a memory allocation error
#define SFS_STATUS_ERR_IO ((sfs_status) 0x0002) ///< A status code for an IO error, whose root cause can be found using `errno()`, `ferror()` or `feof()`

#define SFS_STATUS_ERR_SPACE ((sfs_status) 0x0101) ///< A status code for when there are no more free blocks in the SFS volume
#define SFS_STATUS_ERR_REFNUM ((sfs_status) 0x0102) ///< A status code for when the block reference number exceeds the maximum that can be supported by the filesystem
#define SFS_STATUS_ERR_ISPACE ((sfs_status) 0x0103) ///< A status code for when there are no more free inodes in the SFS volume

#define SFS_STATUS_ERR_IMODE ((sfs_status) 0x0201) ///< A status code for when an invalid operation is invoked on a stream with certain modes
#define SFS_STATUS_ERR_NINODE ((sfs_status) 0x0202) ///< A status code for when an operation is invoked on a NULL inode
#define SFS_STATUS_ERR_NDIR ((sfs_status) 0x0203) ///< A status code for when a directory operation is performed on a non-directory inode

#define SFS_STATUS_ERR_USFORMAT ((sfs_status) 0x0301) ///< A status code for when the formatting params are unsatisfiable

#define SFS_STATUS_ERR_EXISTS ((sfs_status) 0x0401) ///< A status code for when an entry already exists
#define SFS_STATUS_ERR_REMSYSTEM ((sfs_status) 0x0402) ///< A status code for when an illegal attempt at removing a system entry was made
#define SFS_STATUS_ERR_LONGNAME ((sfs_status) 0x0403) ///< A status code for when an illegal attempt at finding a long-named entry was made
#define SFS_STATUS_ERR_NEXISTS ((sfs_status) 0x0404) ///< A status code for when an entry does not exist
#define SFS_STATUS_ERR_NFILE ((sfs_status) 0x0405) ///< A status code for when a datafile operation is attempted on a non-datafile node
#define SFS_STATUS_ERR_NEMPTY ((sfs_status) 0x0406) ///< A status code for when an attempt at removing a non-empty directory was made
#define SFS_STATUS_ERR_NPATH ((sfs_status) 0x0407) ///< A status code for when the path to an entry does not exist

#define SFS_DIR_ENTRY_NAME_LENGTH (12) ///< The maximum length of a directory entry (file) name

/**
 * An opaque data type for referencing of an SFS volume.
 */
typedef void sfs;

/**
 * An opaque data type for referencing of an SFS file stream.
 */
typedef void sfs_stream;

/**
 * A data type for stream modes.
 */
typedef uint8_t sfs_stream_mode;

#define SFS_MODE_READ ((uint8_t) (0x01)) ///< Stream mode that allows reading
#define SFS_MODE_WRITE ((uint8_t) (0x02)) ///< Stream mode that allows writing
#define SFS_MODE_NOREPLACE ((uint8_t) (0x04)) ///< Stream mode that does not replace original data
#define SFS_MODE_END ((uint8_t) (0x08)) ///< Stream mode that starts at the end of the original file
#define SFS_MODE_APPEND ((uint8_t) (SFS_MODE_NOREPLACE | SFS_MODE_END)) ///< Stream mode that appends (conjunction of END and NOREPLACE)

/**
 * A data type for defining the origin of the seek operation.
 */
typedef uint8_t sfs_seek_type;

#define SFS_SEEK_CUR ((uint8_t) 0x00) ///< Sets the current position as the origin
#define SFS_SEEK_SET ((uint8_t) 0x01) ///< Sets the beginning of the file as the origin
#define SFS_SEEK_END ((uint8_t) 0x02) ///< Sets the end of the file as the origin

/**
 * Parameters for SFS formatting.
 */
typedef struct {
    unsigned int inode_count; ///< Number of inodes in the formatted filesystem
    uint32_t block_size; ///< Number of bytes of a single block in the formatted filesystem
    unsigned char clear; ///< Whether the volume should be filled with zeroes before formatting
} sfs_format_params;

/**
 * Gets the thread-local status code.
 *
 * @return the current SFS status code
 */
sfs_status sfs_err();

/**
 * Sets the thread-local status code, if no other status code is currently present.
 *
 * @param [in] status
 *          The status code to set
 *
 * @return the current SFS status code
 */
sfs_status sfs_seterr(sfs_status status);

/**
 * Force-resets the thread-local status code.
 */
void sfs_reseterr();

#endif /*SMORKFS_LIBSFSCOMMON_H*/
