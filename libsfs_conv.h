

#ifndef SMORKFS_LIBSFS_CONV_H
#define SMORKFS_LIBSFS_CONV_H

#include <endian.h>
#include "libsfs_fs_structs.h"

void htof_dir_entry(sfs_dir_entry *entry); ///< Converts the host directory entry at the specified pointer to filesystem directory entry
void ftoh_dir_entry(sfs_dir_entry *entry); ///< Converts the filesystem directory entry at the specified pointer to host directory entry

#endif /* SMORKFS_LIBSFS_CONV_H */
