/**@file
 * @brief Structures for low-level SFS library, to be written to the filesystem.
 */

#ifndef SMORKFS_LIBLOWSFS_FS_STRUCTS_H
#define SMORKFS_LIBLOWSFS_FS_STRUCTS_H

#include <stdint.h>
#include <endian.h>
#include "libsfscommon.h"

typedef uint32_t lsfs_magic_t; ///< A datatype for storing the SFS file's magic number
#define SFS_MAGIC ((lsfs_magic_t) htobe32(0x5F50BEEF)) ///< Magic number for identifying the SFS file, converted to the filesystem's endianness

typedef uint8_t lsfs_inode_type_t; ///< A datatype for signifying the type of an inode

#define LSFS_INODE_TYPE_NULL          ((lsfs_inode_type_t) 0x00) ///< An unused inode
#define LSFS_INODE_TYPE_DIRECTORY     ((lsfs_inode_type_t) 0x01) ///< A directory inode
#define LSFS_INODE_TYPE_FILE          ((lsfs_inode_type_t) 0x02) ///< A data file inode

#define LSFS_INODE_NULL          ((lsfs_inode_id_t) 0) ///< NULL inode ID
#define LSFS_INODE_ROOT          ((lsfs_inode_id_t) 1) ///< The root inode ID

#define LSFS_INODE_NUM_DIRECTS (6u) ///< Number of direct data block references in an inode

typedef uint32_t lsfs_inode_id_t; ///< A datatype for inode IDs and counts
typedef uint64_t lsfs_block_id_t; ///< A datatype for block IDs and counts

#define LSFS_BLOCK_NULL ((lsfs_block_id_t) 0x00) ///< A null block reference

/**
 * The header metadata of the file system.
 */
typedef struct {
    lsfs_magic_t magic; ///< The magic number identifying the file type
    lsfs_inode_id_t inode_count; ///< Number of inodes in this filesystem
    uint32_t block_size; ///< Number of bytes in one block
    lsfs_block_id_t block_count; ///< Number of blocks in this filesystem
} __attribute__((packed)) lsfs_header;

/**
 * A structure of a single inode.
 */
typedef struct {
    lsfs_inode_type_t type; ///< The type of the inode
    uint32_t ref_count; ///< Number of references to this inode (for hardlinks)
    uint64_t size; ///< Size of the file described by the inode
    lsfs_block_id_t blockrefs_d[LSFS_INODE_NUM_DIRECTS]; ///< Direct references to data blocks
    lsfs_block_id_t blockrefs_i1; ///< Reference to a data block containing indirect references to data blocks of this inode
    lsfs_block_id_t blockrefs_i2; ///< Reference to a data block containing indirect references to data blocks containing indirect references to data blocks of this inode
} __attribute__((packed)) lsfs_inode;

#endif /* SMORKFS_LIBLOWSFS_FS_STRUCTS_H */
