/**@file
 * @brief Structures for high-level SFS library, to be written to the filesystem.
 */

#ifndef SMORKFS_LIBSFS_FS_STRUCTS_H
#define SMORKFS_LIBSFS_FS_STRUCTS_H

#include <stdint.h>
#include <endian.h>

#include "liblowsfs.h"
#include "libsfscommon.h"

typedef uint32_t sfs_dir_magic_t; ///< Data type for directory magic number
#define SFS_DIR_MAGIC_V1 ((sfs_dir_magic_t) htobe32(0x5F5D0001)) ///< Magic number for Version 1 SFS directory

typedef uint8_t sfs_dir_entry_type_t; ///< Data type for directory entry type

/**
 * Marks the directory entry as a regular user-created entry
 */
#define SFS_DIR_ENTRY_TYPE_REGULAR ((sfs_dir_entry_type_t) (0x00))

/**
 * Marks the directory entry as a system entry that cannot be deleted by a user and is not counted as directory content
 */
#define SFS_DIR_ENTRY_TYPE_SYSTEM ((sfs_dir_entry_type_t) (0x01))

/**
 * A structure for a single directory entry (file in a directory).
 */
typedef struct {
    uint8_t name[SFS_DIR_ENTRY_NAME_LENGTH]; ///< Name of the entry
    sfs_dir_entry_type_t type; ///< Type of the entry
    lsfs_inode_id_t inode_id; ///< ID of the referenced inode
} __attribute__((packed)) sfs_dir_entry;

#endif /* SMORKFS_LIBSFS_FS_STRUCTS_H */
