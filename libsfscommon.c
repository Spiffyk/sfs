#include "libsfscommon.h"

__thread sfs_status current_status = SFS_STATUS_SUCCESS;

sfs_status sfs_err() {
    return current_status;
}

sfs_status sfs_seterr(sfs_status status) {
    return (current_status) ? (current_status) : (current_status = status);
}

void sfs_reseterr() {
    current_status = SFS_STATUS_SUCCESS;
}
