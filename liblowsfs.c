#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <log.h>

#include "util_macros.h"

#include "liblowsfs_conv.h"
#include "liblowsfs_limits.h"
#include "liblowsfs.h"

uint8_t lsfs_block_used(lsfs_vol *fs, lsfs_block_id_t id);
void lsfs_block_mark(lsfs_vol *fs, lsfs_block_id_t id, uint8_t mark);

lsfs_block_id_t lsfs_block_blockref_get(lsfs_vol *fs, lsfs_block_id_t cont_block, unsigned int block_num);
void lsfs_block_blockref_set(lsfs_vol *fs, lsfs_block_id_t cont_block, unsigned int block_num, lsfs_block_id_t value);

lsfs_vol * lsfs_volume_open(char *path) {
    lsfs_vol *fs = malloc(sizeof(lsfs_vol));
    if (!fs) {
        log_error("Could not allocate fs reference");
        sfs_seterr(SFS_STATUS_ERR_MEMORY);
        return NULL;
    }

    fs->file = fopen(path, "r+");
    if (fs->file == NULL) {
        log_error("Could not open volume file '%s'", path);
        free(fs);
        sfs_seterr(SFS_STATUS_ERR_IO);
        return NULL;
    }

    fseek(fs->file, 0, SEEK_END);
    fs->volume_size = (uint64_t) ftell(fs->file);
    fseek(fs->file, 0, SEEK_SET);

    fs->header_view = NULL;

    lsfs_magic_t current_magic;
    size_t magic_r = fread(&current_magic, sizeof(current_magic), 1, fs->file);
    if (magic_r != 1) {
        log_error("Could not read data from the volume");
        fclose(fs->file);
        free(fs);
        sfs_seterr(SFS_STATUS_ERR_IO);
        return NULL;
    }

    fs->valid = (uint8_t) (current_magic == SFS_MAGIC);
    log_info("Opened SFS volume at '%s' of %ld bytes. It is %s.",
            path, fs->volume_size, (fs->valid) ? "already valid" : "not yet valid");
    return fs;
}


void lsfs_volume_close(lsfs_vol **target) {
    sfs_status header_sync_status = lsfs_header_sync(*target);
    if (header_sync_status) {
        log_error("There was an error syncing the volume's header during volume closure");
    }

    int fcstatus = fclose((*target)->file);
    if (fcstatus == EOF) {
        log_error("There was an error while closing the volume.");
        sfs_seterr(SFS_STATUS_ERR_IO);
    }

    if ((*target)->header_view) {
        free((*target)->header_view);
    }

    free(*target);
    *target = NULL;

    log_info("Closed SFS volume");
}

int lsfs_volume_valid(lsfs_vol *fs) {
    return (fs) ? fs->valid : 0;
}

sfs_status lsfs_format(lsfs_vol *fs, sfs_format_params *params) {
    log_debug("Formatting volume...");
    if (params->block_size < sizeof(lsfs_block_id_t)) {
        log_error("A block of size %ld is too small to fit a block ID (for indirect references)", params->block_size);
        return sfs_seterr(SFS_STATUS_ERR_USFORMAT);
    }

    if (params->clear) {
        log_debug("Clearing volume (filling with zeroes)...");
        fseek(fs->file, 0, SEEK_SET);
        unsigned char zero = 0;
        for (size_t i = 0; i < fs->volume_size;) {
            i += fwrite(&zero, 1, 1, fs->file);
        }
        log_info("Volume cleared (filled with zeroes)");
    }

    fseek(fs->file, 0, SEEK_SET);

    lsfs_header header;
    header.magic = SFS_MAGIC;
    header.inode_count = params->inode_count;
    header.block_size = params->block_size;

    size_t block_space_size = fs->volume_size - sizeof(lsfs_header) - sizeof(lsfs_inode) * header.inode_count;
    lsfs_block_id_t preliminary_block_count = cdiv(block_space_size, header.block_size);
    size_t bitmap_size = cdiv(preliminary_block_count, 8);
    block_space_size -= bitmap_size;
    header.block_count = block_space_size / header.block_size;

    if (lsfs_total_size_h(&header) > fs->volume_size) {
        log_error("The volume is not large enough to satisfy the current formatting parameters");
        return sfs_seterr(SFS_STATUS_ERR_USFORMAT);
    }

    lsfs_header written_header = header;
    htof_header(&written_header);
    size_t header_w = fwrite(&written_header, sizeof(written_header), 1, fs->file);
    if (header_w != 1) {
        log_error("Could not write the volume header");
        fs->valid = 0;
        return sfs_seterr(SFS_STATUS_ERR_IO);
    }

    lsfs_inode empty_inode;
    empty_inode.type = LSFS_INODE_TYPE_NULL;
    empty_inode.ref_count = 0;
    empty_inode.size = 0;
    for (int i = 0; i < LSFS_INODE_NUM_DIRECTS; i++) {
        empty_inode.blockrefs_d[i] = LSFS_BLOCK_NULL;
    }
    empty_inode.blockrefs_i1 = LSFS_BLOCK_NULL;
    empty_inode.blockrefs_i2 = LSFS_BLOCK_NULL;
    htof_inode(&empty_inode);
    for (unsigned int i = 0; i < params->inode_count; i++) {
        size_t inode_w = fwrite(&empty_inode, sizeof(empty_inode), 1, fs->file);
        if (inode_w != 1) {
            log_error("Could not write inode (id: %ld) to the volume", i);
            fs->valid = 0;
            return sfs_seterr(SFS_STATUS_ERR_IO);
        }
    }

    size_t num_bitmap_bytes = lsfs_bitmap_size_h(&header);
    uint8_t empty_byte = 0;
    for (size_t i = 0; i < num_bitmap_bytes; i++) {
        size_t byte_w = fwrite(&empty_byte, sizeof(empty_byte), 1, fs->file);
        if (byte_w != 1) {
            log_error("Could not write the empty bitmap to the volume");
            fs->valid = 0;
            return sfs_seterr(SFS_STATUS_ERR_IO);
        }
    }

    if (ferror(fs->file)) {
        log_error("There was an error during formatting");
        fs->valid = 0;
        return sfs_seterr(SFS_STATUS_ERR_IO);
    } else {
        log_info("Volume successfully formatted");
        fs->valid = 1;
        return sfs_seterr(SFS_STATUS_SUCCESS);
    }
}

lsfs_header *lsfs_header_get(lsfs_vol *fs) {
    if (!fs->header_view) {
        fs->header_view = malloc(sizeof(lsfs_header));
        if (!fs->header_view) {
            sfs_seterr(SFS_STATUS_ERR_MEMORY);
            return NULL;
        }
        fs->header_view->magic = 0;
    }

    if (fs->header_view->magic != SFS_MAGIC) {
        fseek(fs->file, lsfs_header_start(fs), SEEK_SET);
        size_t r = fread(fs->header_view, sizeof(lsfs_header), 1, fs->file);
        if (ferror(fs->file) || r != 1 || fs->header_view->magic != SFS_MAGIC) {
            sfs_seterr(SFS_STATUS_ERR_IO);
            return NULL;
        }

        ftoh_header(fs->header_view);
    }

    return fs->header_view;
}

sfs_status lsfs_header_sync(lsfs_vol *fs) {
    if (!fs->header_view || fs->header_view->magic != SFS_MAGIC) {
        // The header view is currently not valid and thus won't be synced.
        // This is a valid state of the program and not an error!
        log_trace("The volume header was not used, not syncing");
        return sfs_seterr(SFS_STATUS_SUCCESS);
    }

    log_trace("Syncing header...");

    lsfs_header header = *fs->header_view;
    htof_header(&header);

    fseek(fs->file, lsfs_header_start(fs), SEEK_SET);
    size_t w = fwrite(&header, sizeof(lsfs_header), 1, fs->file);
    if (ferror(fs->file) || w != 1) {
        log_error("Could not sync header");
        return sfs_seterr(SFS_STATUS_ERR_IO);
    }

    log_debug("Header synced");
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

sfs_status lsfs_inode_read(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode *out) {
    log_trace("Reading inode (id: %04x)...", id);
    fseek(fs->file, lsfs_inode_start(fs, id), SEEK_SET);
    size_t r = fread(out, sizeof(lsfs_inode), 1, fs->file);
    if (r != 1 || ferror(fs->file) || feof(fs->file)) {
        log_error("Could not read inode (id: %04x)", id);
        return sfs_seterr(SFS_STATUS_ERR_IO);
    }
    ftoh_inode(out);
    log_trace("Read inode (id: %04x)", id);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

sfs_status lsfs_inode_write(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode *in) {
    log_trace("Writing inode (id: %04x)...", id);
    lsfs_inode inode = *in;
    htof_inode(&inode);

    fseek(fs->file, lsfs_inode_start(fs, id), SEEK_SET);
    size_t w = fwrite(&inode, sizeof(lsfs_inode), 1, fs->file);
    if (w != 1 || ferror(fs->file) || feof(fs->file)) {
        log_error("Could not write inode (id: %04x)", id);
        return sfs_seterr(SFS_STATUS_ERR_IO);
    }
    log_trace("Wrote inode (id: %04x)", id);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

void lsfs_inode_init(lsfs_vol *fs, lsfs_inode_id_t id, lsfs_inode_type_t type) {
    lsfs_inode inode;
    inode.type = type;
    inode.ref_count = 0;
    inode.size = 0;
    for (int j = 0; j < LSFS_INODE_NUM_DIRECTS; j++) {
        inode.blockrefs_d[j] = LSFS_BLOCK_NULL;
    }
    inode.blockrefs_i1 = LSFS_BLOCK_NULL;
    inode.blockrefs_i2 = LSFS_BLOCK_NULL;

    lsfs_inode_write(fs, id, &inode);
}

lsfs_inode_id_t lsfs_inode_alloc(lsfs_vol *fs, lsfs_inode_type_t type) {
    if (!fs || !type) {
        return LSFS_INODE_NULL;
    }

    log_trace("Allocating inode...");

    const lsfs_inode_id_t inode_count = lsfs_header_get(fs)->inode_count;
    for (lsfs_inode_id_t i = LSFS_INODE_ROOT + 1; i <= inode_count; i++) {
        lsfs_inode inode;
        lsfs_inode_read(fs, i, &inode);
        if (!inode.type) {
            lsfs_inode_init(fs, i, type);

            log_debug("Allocated and initalized inode (id: %04x)", i);
            return i;
        }
    }

    log_error("Could not allocate an inode - here are no free inodes left");
    sfs_seterr(SFS_STATUS_ERR_ISPACE);
    return LSFS_INODE_NULL;
}

sfs_status lsfs_inode_free(lsfs_vol *fs, lsfs_inode_id_t id) {
    log_trace("Freeing inode (id: %04x)...", id);
    lsfs_inode inode;
    lsfs_inode_read(fs, id, &inode);
    if (sfs_err()) {
        return sfs_err();
    }
    if (!inode.type) {
        log_trace("Inode (id: %04x) was already free, aborting", id);
        return sfs_seterr(SFS_STATUS_SUCCESS);
    }

    {
        const unsigned int max_blocks = lsfs_inode_max_blocks(fs);
        const unsigned int num_indirects = lsfs_num_indirects(fs);

        // Free all data blocks
        for (unsigned int i = 0; i < max_blocks; i++) {
            const lsfs_block_id_t block_id = lsfs_inode_blockref_get(fs, &inode, i);
            if (!block_id) { // We can assume that once we've hit a NULL block, all others will be NULL (it is an error otherwise)
                break;
            }

            lsfs_block_free(fs, block_id);
        }

        // Free level 1 indirect reference block
        if (inode.blockrefs_i1) {
            lsfs_block_free(fs, inode.blockrefs_i1);
        }

        // Free level 2 indirect reference blocks
        if (inode.blockrefs_i2) {
            fseek(fs->file, lsfs_block_start(fs, inode.blockrefs_i2), SEEK_SET);
            for (unsigned int i = 0; i < num_indirects; i++) {
                lsfs_block_id_t block_id = lsfs_block_blockref_get(fs, inode.blockrefs_i2, i);
                if (block_id) {
                    lsfs_block_free(fs, block_id);
                }
            }
            lsfs_block_free(fs, inode.blockrefs_i2);
        }
    }

    inode.type = LSFS_INODE_TYPE_NULL;
    lsfs_inode_write(fs, id, &inode);

    log_debug("Freed inode (id: %04x)", id);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

sfs_status lsfs_inode_ref_inc(lsfs_vol *fs, lsfs_inode_id_t id) {
    lsfs_inode inode;
    lsfs_inode_read(fs, id, &inode);

    if (!inode.type) {
        log_error("Attempted to increase the ref_count of an unallocated inode");
        return sfs_seterr(SFS_STATUS_ERR_NINODE);
    }

    inode.ref_count++;
    lsfs_inode_write(fs, id, &inode);

    log_trace("Inode %04x ref_count incremented to %d", id, inode.ref_count);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

sfs_status lsfs_inode_ref_dec(lsfs_vol *fs, lsfs_inode_id_t id) {
    lsfs_inode inode;
    lsfs_inode_read(fs, id, &inode);

    if (!inode.type) {
        return sfs_seterr(SFS_STATUS_ERR_NINODE);
    }

    if (inode.ref_count) {
        inode.ref_count--;
    }

    if (!inode.ref_count) {
        log_trace("Inode %04x ref_count decremented to %u - no references left, auto-freeing", id, inode.ref_count);
        lsfs_inode_free(fs, id);
    } else {
        log_trace("Inode %04x ref_count decremented to %u", id, inode.ref_count);
        lsfs_inode_write(fs, id, &inode);
    }

    return sfs_err();
}

void lsfs_inode_print_blockrefs(lsfs_vol *fs, lsfs_inode *inode) {
    const unsigned int max_blocks = lsfs_inode_max_blocks(fs);
    for (unsigned int i = 0; i < max_blocks; i++) {
        lsfs_block_id_t block_id = lsfs_inode_blockref_get(fs, inode, i);

        if (block_id) {
            printf("%08lx ", block_id);
        }
    }
}

uint8_t lsfs_block_used(lsfs_vol *fs, lsfs_block_id_t id) {
    if (id <= LSFS_BLOCK_NULL || id > lsfs_header_get(fs)->block_count) {
        return 1;
    }

    id--;

    uint8_t part;
    fseek(fs->file, lsfs_bitmap_start(fs) + ((long) id / 8L), SEEK_SET);
    fread(&part, sizeof(uint8_t), 1, fs->file);

    return part & (uint8_t) (1u << (uint8_t) (id % 8L));
}

void lsfs_block_mark(lsfs_vol *fs, lsfs_block_id_t id, uint8_t mark) {
    if (id <= LSFS_BLOCK_NULL || id > lsfs_header_get(fs)->block_count) {
        return;
    }

    id--;

    uint8_t part;
    fseek(fs->file, lsfs_bitmap_start(fs) + ((long) id / 8L), SEEK_SET);
    fread(&part, sizeof(uint8_t), 1, fs->file);

    uint8_t bit = (uint8_t) (1u << (uint8_t) (id % 8L));
    if (mark) {
        part |= bit;
    } else {
        part &= (uint8_t) ~bit;
    }

    fseek(fs->file, lsfs_bitmap_start(fs) + ((long) id / 8L), SEEK_SET);
    fwrite(&part, sizeof(uint8_t), 1, fs->file);

    log_trace("Marked block %08x as %s", id, (mark) ? "taken" : "free");
}

lsfs_block_id_t lsfs_block_blockref_get(lsfs_vol *fs, lsfs_block_id_t cont_block, unsigned int block_num) {
    if (block_num < 0 || block_num >= lsfs_num_indirects(fs)) {
        sfs_seterr(SFS_STATUS_ERR_REFNUM);
        return LSFS_BLOCK_NULL;
    }

    lsfs_block_id_t result;
    fseek(fs->file, lsfs_block_start(fs, cont_block) + (long) (block_num * sizeof(lsfs_block_id_t)), SEEK_SET);
    fread(&result, sizeof(lsfs_block_id_t), 1, fs->file);
    return ftoh_block_id(result);
}

void lsfs_block_blockref_set(lsfs_vol *fs, lsfs_block_id_t cont_block, unsigned int block_num, lsfs_block_id_t value) {
    if (block_num < 0 || block_num >= lsfs_num_indirects(fs)) {
        sfs_seterr(SFS_STATUS_ERR_REFNUM);
        return;
    }

    value = htof_block_id(value);
    fseek(fs->file, lsfs_block_start(fs, cont_block) + (long) (block_num * sizeof(lsfs_block_id_t)), SEEK_SET);
    fwrite(&value, sizeof(lsfs_block_id_t), 1, fs->file);
}

lsfs_block_id_t lsfs_inode_blockref_get(lsfs_vol *fs, lsfs_inode *inode, unsigned int block_num) {
    unsigned int num_i = lsfs_num_indirects(fs);

    if (block_num < LSFS_INODE_NUM_DIRECTS) {
        return inode->blockrefs_d[block_num];
    }

    unsigned int i1_datablock_num = block_num - LSFS_INODE_NUM_DIRECTS;
    if (i1_datablock_num < num_i) {
        if (!inode->blockrefs_i1) {
            return LSFS_BLOCK_NULL;
        }

        return lsfs_block_blockref_get(fs, inode->blockrefs_i1, i1_datablock_num);
    }

    unsigned int i2_block_num = i1_datablock_num - num_i;
    unsigned int i2_refblock_num = i2_block_num / num_i;
    unsigned int i2_datablock_num = i2_block_num % num_i;
    if (i2_refblock_num < num_i) {
        if (!inode->blockrefs_i2) {
            return LSFS_BLOCK_NULL;
        }

        lsfs_block_id_t lv2 = lsfs_block_blockref_get(fs, inode->blockrefs_i2, i2_refblock_num);
        if (!lv2) {
            return LSFS_BLOCK_NULL;
        }

        return lsfs_block_blockref_get(fs, lv2, i2_datablock_num);
    }

    sfs_seterr(SFS_STATUS_ERR_REFNUM);
    return LSFS_BLOCK_NULL;
}

void lsfs_inode_blockref_set(lsfs_vol *fs, lsfs_inode *inode, unsigned int block_num, lsfs_block_id_t value) {
    unsigned int num_i = lsfs_num_indirects(fs);

    if (block_num < LSFS_INODE_NUM_DIRECTS) {
        inode->blockrefs_d[block_num] = value;
        return;
    }

    unsigned int i1_datablock_num = block_num - LSFS_INODE_NUM_DIRECTS;
    if (i1_datablock_num < num_i) {
        if (!inode->blockrefs_i1) {
            if (!value) {
                return;
            }
            inode->blockrefs_i1 = lsfs_block_alloc(fs);
        }

        lsfs_block_blockref_set(fs, inode->blockrefs_i1, i1_datablock_num, value);
        return;
    }

    unsigned int i2_block_num = i1_datablock_num - num_i;
    unsigned int i2_refblock_num = i2_block_num / num_i;
    unsigned int i2_datablock_num = i2_block_num % num_i;
    if (i2_refblock_num < num_i) {
        if (!inode->blockrefs_i2) {
            if (!value) {
                return;
            }
            inode->blockrefs_i2 = lsfs_block_calloc(fs);
        }

        lsfs_block_id_t lv2 = lsfs_block_blockref_get(fs, inode->blockrefs_i2, i2_refblock_num);
        if (!lv2) {
            if (!value) {
                return;
            }
            lv2 = lsfs_block_calloc(fs);
            lsfs_block_blockref_set(fs, inode->blockrefs_i2, i2_refblock_num, lv2);
        }

        lsfs_block_blockref_set(fs, lv2, i2_datablock_num, value);
        return;
    }

    sfs_seterr(SFS_STATUS_ERR_REFNUM);
}

lsfs_block_id_t lsfs_block_alloc(lsfs_vol *fs) {
    lsfs_header *header = lsfs_header_get(fs);
    for (lsfs_block_id_t id = 1; id <= header->block_count; id++) {
        if (!lsfs_block_used(fs, id)) {
            lsfs_block_mark(fs, id, 1);
            return id;
        }
    }

    sfs_seterr(SFS_STATUS_ERR_SPACE);
    return LSFS_BLOCK_NULL;
}

lsfs_block_id_t lsfs_block_calloc(lsfs_vol *fs) {
    lsfs_block_id_t result = lsfs_block_alloc(fs);

    fseek(fs->file, lsfs_block_start(fs, result), SEEK_SET);
    for (unsigned int i = 0; i < lsfs_header_get(fs)->block_size; i++) {
        unsigned char zero = 0;
        fwrite(&zero, 1, 1, fs->file);
    }

    return result;
}

sfs_status lsfs_block_free(lsfs_vol *fs, lsfs_block_id_t id) {
    if (!id) {
        return sfs_seterr(SFS_STATUS_SUCCESS);
    }
    lsfs_block_mark(fs, id, 0);
    return sfs_seterr(SFS_STATUS_SUCCESS);
}

unsigned int lsfs_count_used_blocks(lsfs_vol *fs) {
    unsigned int result = 0;

    lsfs_block_id_t bc = lsfs_header_get(fs)->block_count;
    for (lsfs_block_id_t i = 1; i <= bc; i++) {
        if (lsfs_block_used(fs, i)) {
            result++;
        }
    }

    return result;
}

lsfs_stream *lsfs_open(lsfs_vol *fs, lsfs_inode_id_t id, sfs_stream_mode mode) {
    if (!fs || !id || !mode) {
        return NULL;
    }

    log_trace("Opening inode %04x", id);

    lsfs_header *header = lsfs_header_get(fs);
    if (id >= header->inode_count) {
        return NULL;
    }

    lsfs_inode inode;
    sfs_status rstatus = lsfs_inode_read(fs, id, &inode);
    if (rstatus || !inode.type) {
        return NULL;
    }

    lsfs_stream *stream = malloc(sizeof(lsfs_stream));
    stream->volume = fs;
    stream->inode = inode;
    stream->inode_id = id;
    stream->mode = mode;

    if ((mode & SFS_MODE_WRITE) && !(mode & SFS_MODE_NOREPLACE)) {
        stream->inode.size = 0;
    }

    stream->pos = (mode & SFS_MODE_END) ? (long) (inode.size) : 0L;

    log_trace("Opened inode %04x", id);
    return stream;
}

void lsfs_close(lsfs_stream *stream) {
    if (!stream) {
        return;
    }

    lsfs_inode_id_t inode_id = stream->inode_id;
    log_trace("Closing inode %04x...", inode_id);

    if ((stream->mode & SFS_MODE_WRITE)) {
        lsfs_header *header = lsfs_header_get(stream->volume);
        unsigned int i = (unsigned int) cdiv(stream->inode.size, header->block_size);
        unsigned int max_blocks = lsfs_inode_max_blocks(stream->volume);
        for (; i < max_blocks; i++) {
            lsfs_block_free(stream->volume, lsfs_inode_blockref_get(stream->volume, &stream->inode, i));
            lsfs_inode_blockref_set(stream->volume, &stream->inode, i, LSFS_BLOCK_NULL);
        }

        lsfs_inode_write(stream->volume, inode_id, &stream->inode);
        fflush(stream->volume->file);
    }

    free(stream);
    log_trace("Closed inode %04x", inode_id);
}

void lsfs_seek(lsfs_stream *stream, long seek, sfs_seek_type type) {
    switch(type) { // NOLINT(hicpp-multiway-paths-covered)
        case SFS_SEEK_SET:
            stream->pos = seek;
            break;
        case SFS_SEEK_CUR:
            stream->pos += seek;
            break;
        case SFS_SEEK_END:
            stream->pos = (long) stream->inode.size + seek;
            break;
        default:
            log_warn("Unknown seek type %d, skipping", type);
    }
}

long lsfs_tell(const lsfs_stream *stream) {
    return stream->pos;
}

int lsfs_eof(const lsfs_stream *stream) {
    return stream->pos < 0 || stream->pos >= stream->inode.size;
}

long lsfs_trim(lsfs_stream *stream) {
    if (!(stream->mode & SFS_MODE_WRITE)) {
        sfs_seterr(SFS_STATUS_ERR_IMODE);
        return stream->inode.size;
    }

    if (stream->pos < 0) {
        stream->inode.size = 0;
    } else if (stream->pos < stream->inode.size) {
        stream->inode.size = (uint64_t) stream->pos;
    }

    log_trace("Trimmed inode %04x size to %lu", stream->inode_id, stream->inode.size);
    return stream->inode.size;
}

size_t lsfs_write(const void *src, size_t elem_size, size_t elem_count, lsfs_stream *stream) {
    if (!(stream->mode & SFS_MODE_WRITE)) {
        sfs_seterr(SFS_STATUS_ERR_IMODE);
        return 0;
    }

    if (stream->pos < 0 || stream->pos > stream->inode.size) {
        return 0;
    }

    lsfs_vol *fs = stream->volume;
    const size_t block_size = lsfs_header_get(fs)->block_size;
    size_t total_data = elem_size * elem_count;
    size_t remaining_data = total_data;
    size_t written_data = 0;

    while (remaining_data > 0) {
        const unsigned int block_ord = (unsigned int) (stream->pos / block_size);
        lsfs_block_id_t block_id = lsfs_inode_blockref_get(fs, &stream->inode, block_ord);
        if (!block_id) {
            if (sfs_err()) {
                break;
            }

            block_id = lsfs_block_alloc(fs);
            if (!block_id) {
                break;
            }
            lsfs_inode_blockref_set(fs, &stream->inode, block_ord, block_id);
        }

        const size_t block_remaining_data = block_size - (stream->pos % block_size);
        const size_t data_to_write = (remaining_data <= block_remaining_data) ? remaining_data : block_remaining_data;

        fseek(fs->file, lsfs_block_start(fs, block_id) + (long) (stream->pos % block_size), SEEK_SET);
        const size_t fwr = fwrite(src, 1, data_to_write, fs->file);

        if (fwr != data_to_write) {
            log_error("Could not write data to block %08lx", block_id);
            sfs_seterr(SFS_STATUS_ERR_IO);
            break;
        }
        log_trace("Wrote %lu (of %lu total) bytes of data to block %08lx", data_to_write, total_data, block_id);

        remaining_data -= fwr;
        written_data += fwr;
        stream->pos += fwr;
        src += fwr;
        if (stream->pos > stream->inode.size) {
            stream->inode.size = (uint64_t) stream->pos;
        }
    }

    return written_data / elem_size;
}

size_t lsfs_read(void *dest, size_t elem_size, size_t elem_count, lsfs_stream *stream) {
    if (!(stream->mode & SFS_MODE_READ)) {
        sfs_seterr(SFS_STATUS_ERR_IMODE);
        return 0;
    }

    if (lsfs_eof(stream)) {
        return 0;
    }

    lsfs_vol *fs = stream->volume;
    const size_t block_size = lsfs_header_get(fs)->block_size;
    size_t total_size = elem_size * elem_count;
    size_t remaining_size = stream->inode.size - stream->pos;

    size_t total_data = (remaining_size <= total_size) ? remaining_size : total_size;
    size_t remaining_data = total_data;
    size_t read_data = 0;

    while (remaining_data > 0) {
        const unsigned int block_ord = (unsigned int) (stream->pos / block_size);
        lsfs_block_id_t block_id = lsfs_inode_blockref_get(fs, &stream->inode, block_ord);
        if (lsfs_eof(stream) || !block_id) {
            break;
        }

        const size_t block_remaining_data = block_size - (stream->pos % block_size);
        const size_t data_to_read = (remaining_data <= block_remaining_data) ? remaining_data : block_remaining_data;

        fseek(fs->file, lsfs_block_start(fs, block_id) + (long) (stream->pos % block_size), SEEK_SET);
        const size_t fwr = fread(dest, 1, data_to_read, fs->file);
        if (fwr != data_to_read) {
            log_error("Could not read data from block %08lx", block_id);
            sfs_seterr(SFS_STATUS_ERR_IO);
            break;
        }
        log_trace("Read %lu (of %lu total) bytes of data from block %08lx", data_to_read, total_data, block_id);

        remaining_data -= fwr;
        read_data += fwr;
        stream->pos += fwr;
        dest += fwr;
    }

    return read_data / elem_size;
}


